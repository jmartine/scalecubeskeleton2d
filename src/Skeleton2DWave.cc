#include "Skeleton2DWave.h"

Skeleton2DWave::Skeleton2DWave(Skeleton2DEdge* SE1, Skeleton2DEdge* SE2, Skeleton2DGraph::VertexId GRAPHID) : 
 se1(SE1), se2(SE2), use_edges(true), lowerOrdinate(false), constant(false), graphId(GRAPHID){}

Skeleton2DWave::Skeleton2DWave(Skeleton2DEdge* SE1, Skeleton2DEdge* SE2, Number Y, Number LOWY, Number UPY, Skeleton2DGraph::VertexId GRAPHID) :
se1(SE1), se2(SE2), y(Y), lowy(LOWY), upy(UPY), use_edges(true), lowerOrdinate(false), constant(true), graphId(GRAPHID){}

Skeleton2DWave::Skeleton2DWave(Number Y, bool LOWERORDINATE):  y(Y), use_edges(false),  lowerOrdinate(LOWERORDINATE), graphId(NULL){}

std::ostream& operator<<(std::ostream& out, const Skeleton2DWave &sw){
	return out <<"["<<sw.getEdge1()->getId()<<","<<sw.getEdge2()->getId()<<"]";
}
