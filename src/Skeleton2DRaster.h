#ifndef SKELETON2DRASTER
#define SKELETON2DRASTER

#include <string>
#include <boost/multi_array.hpp>
#include <fstream>
#include "Skeleton2DGlobals.h"

using namespace std;

class Skeleton2DRaster{

	public:
		#define EMPTY 0
		#define SKELETON 1
		#define BORDER 2


		Skeleton2DRaster(int xmin, int ymin, int xmax, int ymax);
		
		//! \brief Adds a vertex defined by its integer coordinates
		//! \param ax integer x-coordinate  of the rastered vertex
		//! \param ay integer y-coordinate of the rastered vertex
		void addVertex(int ax, int ay, int c);
		
		//! \brief Adds an edge defined by its Number coordinates. Proper truncation is done to guarantee correct connectivity
		//! \param ax x-coordinate of the first vertex
		//! \param ay y-coordinate of the first vertex
		//! \param bx x-coordinate of the second vertex
		//! \param by y-coordinate of the second vertex
		void addEdge(double ax, double ay, double bx, double by, int c);
		
		//! \brief Exports the rasterization to a file
		//! \param Output path
		void exportRaster(string path);
		~Skeleton2DRaster();
	private:
		int offsetx, offsety;
		int nx, ny;

		typedef boost::multi_array<int, 2> bitmap_type;
		typedef bitmap_type::index index;
		bitmap_type bitmap;
};

#endif

