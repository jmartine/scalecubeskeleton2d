#ifndef __SKELETON2DGRAPH_H
#define __SKELETON2DGRAPH_H

#include <utility> 
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>
#include <limits>
#include <float.h>
#include <map>
#include "Skeleton2DGlobals.h"
#include "Skeleton2DRaster.h"

using namespace std;

struct VertexInfo {
	Vertex vertex;
	Number radius;
};

class Skeleton2DGraph {
	private:
		typedef boost::property<boost::vertex_color_t, VertexInfo, 
		        boost::property<boost::vertex_index_t, int> > VertexProperties;
		typedef boost::adjacency_list< boost::setS,  boost::listS,  boost::undirectedS, VertexProperties, boost::disallow_parallel_edge_tag> Graph;
		typedef boost::property_map<Graph, boost::vertex_color_t>::type VertexMap;
		typedef boost::property_map<Graph, boost::vertex_index_t>::type IndexMap;
		VertexMap vertexMap;
		IndexMap indexMap;
		Graph graph;
		void addBoxExtremeFormat(std::vector<double* >& boxes, Vertex& v, Number radius, double mul);
	public:
		Skeleton2DGraph();
		~Skeleton2DGraph();
		typedef boost::graph_traits<Graph>::vertex_descriptor VertexId;
		typedef boost::graph_traits<Graph>::vertex_iterator vertex_iter;
		typedef std::pair<VertexId,VertexId> VertexPair;
		typedef std::vector<VertexPair> VertexPairList;
		typedef std::map<std::pair<Number,Number>,std::set<VertexId> > TypeMapVertexCoordPair;
		TypeMapVertexCoordPair mapVertexCoordPair;
		
		//! \brief Adds an edge to the graph defined by two vertices
		//! \param id1 First vertex descriptor
		//! \param id2 Second vertex descriptor
		void addEdge(VertexId id1, VertexId id2);
		
		//! \brief Adds a vertex to the graph defined by its coordinates
		//! \param x x-coordinate of the vertex
		//! \param y y-coordinate of the vertex
		//! \return a descriptor of the inserted vertex
		VertexId addVertex(Number x, Number y, Number radius);
		VertexId addVertex(Vertex v, Number radius);
		
		//! \brief Adds a vertex to the graph defined by its coordinates and connected to another vertex
		//! \param vid Connected vertex
		//! \param x x-coordinate of the vertex
		//! \param y y-coordinate of the vertex
		//! \return a descriptor of the inserted vertex
		VertexId addConnectedVertex1Parent(VertexId vid, Number x, Number y, Number radius);
		
		//! \brief Adds a vertex to the graph defined by its coordinates and connected to another two vertices
		//! \param vid1 First connected vertex
		//! \param vid2 Second connected vertex
		//! \param x x-coordinate of the vertex
		//! \param y y-coordinate of the vertex
		//! \return a descriptor of the inserted vertex
		VertexId addConnectedVertex2Parent(VertexId vid1,VertexId vid2, Number x, Number y, Number radius);
		
		//! \brief Returns the next inner vertex in the graph given an starting vertex and a direction
		//! \param vid Initial vertex descriptor
		//! \param vertical True if vertical sweep and false if horitzontal one
		//! \param up True if up direction and false if down one 
		//! \param right True if right direction and false if left one
		//! \param first Indicates if is the first search of the next inner vertex
		//! \return the next inner last vertex
		VertexId getInnerLastVertex(VertexId vid, bool vertical, bool up, bool right, bool first);
		
		//! \brief Returns the vertex degree of an input vertex
		//! \param vid Input vertex descriptor
		//! \return the vertex degree of the input vertex descriptor
		int getVertexDegree(VertexId vid);
		
		//! \brief Returns the vertex coordinate associated to a vertex descriptor
		//! \param vid Input vertex descriptor
		//! \return the vertex coordinate of the input vertex descriptor
		VertexInfo getVertex(VertexId vid);
		
		//! \return the number of vertices of the graph
		int getNumVertices();
		
		//! \return the number of edges of the graph
		int getNumEdges();
		
		#ifdef DEBUG
		void printInfo();
		#endif
		
		//! \brief Removes all the graph vertices with the input degree
		void pruneGraph(int degree);
		
		//! \brief Repairs the vertex coincidences of the graph removing 
		void repairCoincidences();
		
		//! \brief Repairs the collinear ambiguities of the graph
		//! \param true if repairs in edge mode and false if use vertex mode
		void repairCollinearities(bool edgeMode);
		
		//! \brief Removes the edge defined by two input vertices
		//! \param id1 First vertex descriptor
		//! \param id2 Second vertex descriptor
		void removeEdge(VertexId id1, VertexId id2);
		
		//! \brief Remove the vertex defined by an input vertex descriptor
		//! \param vid The vertex descriptor to remove
		void removeVertex(VertexId vid);
		
		void saveBoxes(double multDistScale, double shift);
		void outputSvg(string path, Number xmax, Number ymax, bool close, double shift);
		void addToRaster(Skeleton2DRaster* raster, double shift);
	private:
		bool repairType(VertexId vidn1, bool edgeMode, bool normalIteration, unsigned int& numRepaired);
		void insertVertexCoord(Number x, Number y, VertexId vid);
};

#endif
