#include "Skeleton2DEventSegment.h"

Skeleton2DEventSegment::Skeleton2DEventSegment(Number X, Skeleton2DEdge* EDGE) : type(SITE_EVENT), edge(EDGE){
	x=X;
}

Skeleton2DEventSegment::Skeleton2DEventSegment(Number* DI, Skeleton2DWaveSP const& SW1, Skeleton2DWaveSP const& SW2) : type(SPIKE_EVENT){
	x=DI[1];
	for (int i=0; i<6; i++) Di[i]=DI[i];
	sw1=SW1;
	sw2=SW2;
	classifyEdges(sw1,sw2,sharedEdge,edge1,edge2);
}

bool Skeleton2DEventCompareSegment::operator()(Skeleton2DEventSegment& se1, Skeleton2DEventSegment& se2){
	if (se1.getX()==se2.getX()){
		if ( ((se1.getType()==Skeleton2DEventSegment::SITE_EVENT)&&(se2.getType()==Skeleton2DEventSegment::SITE_EVENT)) || 
		     ((se1.getType()==Skeleton2DEventSegment::SPIKE_EVENT)&&(se2.getType()==Skeleton2DEventSegment::SPIKE_EVENT)) ) {
				return se1.getMinY() < se2.getMinY();
		}else {
			return (se2.getType()==Skeleton2DEventSegment::SPIKE_EVENT);
		}
	}else {
		return (se1.getX()>se2.getX());
	}
}

std::ostream& operator<<(std::ostream& out, const Skeleton2DEventSegment &se){
	if (se.getType()==Skeleton2DEventSegment::SITE_EVENT){
		return out <<"SITE EVENT x="<<se.getX()<<" Edge ID="<<se.getEdge()->getId();
	}else if (se.getType()==Skeleton2DEventSegment::SPIKE_EVENT){
		return out <<"SPIKE EVENT x="<<se.getX()<<" Wave 1="<<*(se.getWave1())<<" Wave 2="<<*(se.getWave2());
	}else return out;
}

Number Skeleton2DEventSegment::getMinY(){
	if (this->type == SITE_EVENT) return edge->getMinY();
	else return min(min(sharedEdge->getMinY(), edge1->getMinY()), edge2->getMinY());
}
