#include <iostream>
#include <limits>
#include <getopt.h>
#include "Skeleton2DParams.h"
#include "Skeleton2D.h"
#include "Skeleton2DSegment.h"
#include "Skeleton2DPoint.h"

using namespace std;

void printSyntax(){
	cerr << "-h --help"<<endl;
	cerr << "-m --mode_point"<<endl;
	cerr << "-v --voronoi_diagram"<<endl;
	cerr << "-r --output_polygon"<<endl;
	cerr << "-i --input_file <Input brep model>"<<endl;
	cerr << "-s --svg <route to output svg file>"<<endl;
	cerr << "-p --ppm <route to output ppm file>"<<endl;
	cerr << "-b --bound <boundx,boundy> bounding box" << endl;
	cerr << "-f --scale"<<endl;
}

void printHelp(){
	cout<<"2Dskeleton computes the L infinity Voronoi diagram and the interior cube skeleton of orthogonal polygons"<<endl;
	cout<<"Usage: 2Dskeleton -hmvr -i [input file (txt,brep)] -g [output svg file] -m [output ppm file] -b [boundx,boundy] -f [scale]"<<endl;
	printSyntax();
}

void print_syntax_error(){
	cerr << "Syntax Error:\n";
	printSyntax();
}

int main(int argc, char *argv[]){
	Skeleton2DParams params = Skeleton2DParams();
	static struct option long_options[] ={
		{"help", no_argument, 0, 'h'},
		{"mode_point", no_argument, 0, 'm'},
		{"voronoi_diagram", no_argument, 0, 'v'},
		{"input_file", required_argument, 0, 'i'},
		{"svg", required_argument, 0, 's'},
		{"ppm", required_argument, 0, 'p'},
		{"bound", required_argument, 0, 'b'},
		{"scale", required_argument, 0, 'f'},
		{"output_polygon", no_argument, 0, 'r'},
		{0, 0, 0, 0}
	};
	int option_index = 0;
	int c;
	while ((c = getopt_long (argc, argv, "hmvri:s:p:b:f:", long_options, &option_index)) != -1){
		switch (c){
			case 'h':
				printHelp();
				return 0;
			case 'm':
				params.pointMode = true;
				break;
			case 'v':
				params.cubeskeleton=false;
				break;
			case 'r':
				params.outputBorders=true;
				break;
			case 'i':
				params.filename = optarg;
				break;
			case 's':
				params.svgOutput=optarg;
				break;
			case 'p':
				params.ppmOutput=optarg;
				break;
			case 'b': {
				string arg(optarg);
				int comma = arg.find(",");
				params.boundx = convertToInt(arg.substr(0, comma));
				params.boundy = convertToInt(arg.substr(comma+1));
				break;
			}
			case 'f':
				params.multDistScale = atof(optarg);
				break;
			default:
				print_syntax_error();
				return -1;
		}
	}
	cout.precision(numeric_limits< double >::digits10);
	Skeleton2D* skeleton2d;
	if (params.pointMode) skeleton2d = new Skeleton2DPoint(params);
	else skeleton2d = new Skeleton2DSegment(params);
	skeleton2d->initialize();
	int ret = (not skeleton2d->isValid());
	delete skeleton2d;
	return ret;
}
