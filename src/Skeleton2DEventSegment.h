#ifndef __SKELETONEVENTSEGMENT_H
#define __SKELETONEVENTSEGMENT_H

#include "Skeleton2DEvent.h"

class Skeleton2DEventSegment : public Skeleton2DEvent{
	public:
		enum Skeleton2DEventTypeSegment {SITE_EVENT, SPIKE_EVENT};
		friend std::ostream& operator<<(std::ostream& out, const Skeleton2DEventSegment& se);
		
		//! \return the type of event
		enum Skeleton2DEventTypeSegment getType() const {return type;}
		
		//! \return the site event edge
		Skeleton2DEdge* getEdge() const {return edge;}
		
		//! \brief Constructor of site event
		//! \param x Event priority
		//! \param edge Edge associated to the event
		Skeleton2DEventSegment(Number x, Skeleton2DEdge* edge);
		
		//! \brief Constructor of spike event
		//! \param Di Event square
		//! \param sw1 First inducing wave
		//! \param sw2 Second inducing wave
		Skeleton2DEventSegment(Number* Di, Skeleton2DWaveSP const& sw1, Skeleton2DWaveSP const& sw2);
		
		//! \return the minimum event y coordinate
		Number getMinY();
	private:
		enum Skeleton2DEventTypeSegment type;
		Skeleton2DEdge *edge;
};

class Skeleton2DEventCompareSegment{
    public:
		bool operator()(Skeleton2DEventSegment& se1, Skeleton2DEventSegment& se2);
};

#endif
