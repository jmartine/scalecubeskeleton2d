#ifndef __SKELETON2DLINE_H
#define __SKELETON2DLINE_H

#include <iostream>
#include "Skeleton2DGlobals.h"

class Skeleton2DLine{
	private:
		Number a,b,c;              	
		bool positiveFace;			//positive/negative facing
		friend std::ostream& operator<<(std::ostream& out, const Skeleton2DLine& sl);
	public:
		//! \brief Constructor of two dimensional oriented line ax+by+c
		//! \param a coefficient
		//! \param b coefficient
		//! \param c coefficient
		//! \param positiveFace Line orientation
		Skeleton2DLine(Number a, Number b, Number c, bool positiveFace);
		
		//! \return the cofficient a of the line
		Number getA() const { return a;}
		
		//! \return the cofficient b of the line
		Number getB() const { return b;}
		
		//! \return the cofficient c of the line
		Number getC() const { return c;}
		
		//! \brief sets the cofficient c of the line
		void setC(Number val) {this->c=val;}
		
		//! \return the orientation of the line
		bool isPositiveFace() const {return positiveFace;}
};

#endif
