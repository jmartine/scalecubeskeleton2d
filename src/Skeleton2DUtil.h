#ifndef __SKELETON2DUTIL_H
#define __SKELETON2DUTIL_H

#include <iostream>
#include <sstream>
#include <string>
#include "Skeleton2DLine.h"
#include "Skeleton2DEdge.h"
#include "Skeleton2DWave.h"

#define min(X, Y)  ((X) < (Y) ? (X) : (Y))
#define max(X, Y)  ((X) > (Y) ? (X) : (Y))

double convertTodouble(const std::string& s);
int convertToInt(const std::string& s);
Number convertToNumber(const string& s, Number shift);

//! \brief Computes the square defined by three lines
//! \param sl1 First line 
//! \param sl2 Second line 
//! \param sl3 Third line 
//! \param square The computed square
//! \return true if an square defined by the three lines exists
bool getSquare3Lines(Skeleton2DLine* sl1, Skeleton2DLine* sl2, Skeleton2DLine* sl3, Number* square);

//! \brief Computes the square defined by three edges lines
//! \param se1 First edge
//! \param se2 Second edge
//! \param se3 Third edge
//! \param square The computed square
//! \return true if an square defined by the three edges exists
bool getSquare3Edges(Skeleton2DEdge* se1, Skeleton2DEdge* se2, Skeleton2DEdge* se3, Number* square);

//! \brief Returns the square defined a wave and a line
//! \param sw The input wave
//! \param sl The input line
//! \param square The computed square
//! \return true if an square defined by the wave and line exists
bool getSquareWaveLine(Skeleton2DWaveSP const& sw, Skeleton2DLine* sl, Number* square);

//! \brief Returns the square defined two waves that must share an edge
//! \param sw1 First wave
//! \param sw2 Second wave
//! \param square The computed square
//! \return true if an square defined by the two waves exists
bool getSquare2Waves(Skeleton2DWaveSP const& sw1, Skeleton2DWaveSP const& sw2, Number* square);

//! \brief Returns the square defined two edges and a line
//! \param se1 First edge
//! \param se2 Second edge
//! \param sl The input line
//! \param square The computed square
//! \return true an square defined by the two edges and the line exists
bool getSquare2EdgesLine(Skeleton2DEdge* se1, Skeleton2DEdge* se2, Skeleton2DLine* sl, Number* square);

//! \return the vertex shared between two edges
Skeleton2DVertex* getSharedVertex(Skeleton2DEdge* se1, Skeleton2DEdge* se2);

//! \return the vertex shared between the two edges of a wave
Skeleton2DVertex* getSharedVertex(Skeleton2DWaveSP const& sw);

bool getOrigen(Skeleton2DEdge* se1, Skeleton2DEdge* se2, Skeleton2DEdge* se3, Number &y);

//! \return true it the two input waves share an edge and otherwise false
bool wavesShareEdge(Skeleton2DWaveSP const& sw1, Skeleton2DWaveSP const& sw2);

//! \return true if the two input waves are equal and otherwise false
bool equalWaves(Skeleton2DWaveSP const& sw1, Skeleton2DWaveSP const& sw2);

//! \return true if the input wave is equal to the wave induced by the two input edges and otherwise false
bool equalWaveEdges(Skeleton2DWaveSP const& sw,Skeleton2DEdge* se1, Skeleton2DEdge* se2);

//! \return the shared edge between two waves, and the other two different edges
void classifyEdges(Skeleton2DWaveSP const& sw1, Skeleton2DWaveSP const& sw2, Skeleton2DEdge* &sharedEdge, Skeleton2DEdge* &edge1, Skeleton2DEdge* &edge2);

//! \return the shared edge between two waves
void classifyEdges(Skeleton2DWaveSP const& sw1, Skeleton2DWaveSP const& sw2, Skeleton2DEdge* &sharedEdge);

#endif
