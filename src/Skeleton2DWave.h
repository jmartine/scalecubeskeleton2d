#ifndef __SKELETON2DWAVE_H
#define __SKELETON2DWAVE_H

#include <iostream>
#include <vector>
#include <memory>

#include "Skeleton2DEdge.h"

//! \brief A wave corresponds to a wavefront element and is sliding along two tracks which are either spike bisectors or segments
class Skeleton2DWave{
	private:
		Skeleton2DEdge *se1,*se2; //arbitrary edge order
		Number y, lowy, upy;
		bool use_edges, lowerOrdinate, constant;
		Skeleton2DGraph::VertexId graphId;
	public:
		//! \brief Standard wave constructor (two edges defining an square with sweepline) 	
		Skeleton2DWave(Skeleton2DEdge* se1, Skeleton2DEdge* se2, Skeleton2DGraph::VertexId graphId);
		
		//! \brief Constant wave constructor (temporal waves)
		Skeleton2DWave(Skeleton2DEdge* se1, Skeleton2DEdge* se2, Number y, Number lowy, Number upy, Skeleton2DGraph::VertexId graphId);
		
		//! \brief Point query constructor
		Skeleton2DWave(Number y, bool lowerOrdinate);
		
		bool useEdges() const{ return this->use_edges;}
		bool isLowerOrdinate() const{ return this->lowerOrdinate;}
		bool isConstant() const { return constant;}
		Skeleton2DGraph::VertexId getGraphId(){ return graphId;}
		void setGraphId(Skeleton2DGraph::VertexId GRAPHID){this->graphId=GRAPHID;}
		Number getY() const { return y;}
		Number getLowy() const { return upy;}
		Number getUpy() const { return lowy;}
		Skeleton2DEdge* getEdge1() const { return se1;}
		Skeleton2DEdge* getEdge2() const { return se2;}	
		friend std::ostream& operator<<(std::ostream& out, const Skeleton2DWave& sw);
};

typedef boost::shared_ptr<Skeleton2DWave > Skeleton2DWaveSP;

#endif
