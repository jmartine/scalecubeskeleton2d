#include "Skeleton2DLine.h"

Skeleton2DLine::Skeleton2DLine(Number A, Number B, Number C, bool POSITIVEFACE) : a(A), b(B), c(C), positiveFace(POSITIVEFACE){}

std::ostream& operator<<(std::ostream& out, const Skeleton2DLine &sl){
	return out <<"a="<<sl.getA()<<" b="<<sl.getB()<<" c="<<sl.getC()<<" positive="<<sl.isPositiveFace();
}
