#include "Skeleton2DPoint.h"

extern Skeleton2DLine* sweepline;

Skeleton2DPoint::~Skeleton2DPoint(){
	delete events;
}

//sw1 < sw2  sw1 in earlier position than sw2
bool compareWavePoint (Skeleton2DWaveSP sw1, Skeleton2DWaveSP sw2) {
	if ((!sw1->useEdges())&&(!sw2->useEdges()) ){
		std::cout<<"!ERROR compareWave Unable to compare two points (wave required)"<<std::endl;
		exit(-1);
	}
	if (sw1->useEdges() && sw2->useEdges()){
		Number cy1, Di[6];
		if (sw1->isConstant()) cy1=sw1->getY();
		else{
			getSquareWaveLine(sw1, sweepline, Di);
			cy1 = Di[5]; 
		}
		Number cy2;
		if (sw2->isConstant()) cy2=sw2->getY();
		else{
			getSquareWaveLine(sw2, sweepline, Di);             
			cy2 = Di[5]; 
		}
		return (cy1 < cy2);
	}else{
		Skeleton2DWaveSP sw;
		Number y_square, y_comp;
		bool lowerOrdinate;
		if (sw1->useEdges()){
			sw = sw1;
			y_comp = sw2->getY();
			lowerOrdinate = sw2->isLowerOrdinate();
		}else{
			sw = sw2;
			y_comp = sw1->getY();
			lowerOrdinate= sw1->isLowerOrdinate();
		}
		if (sw->isConstant()){
			if (lowerOrdinate) y_square = sw->getLowy();
			else y_square = sw->getUpy();
		}else{	
			Number Di[6];
			if (getSquareWaveLine(sw,sweepline,Di)){
				if (lowerOrdinate) y_square = Di[2];
				else y_square = Di[3];
				if (sw1->useEdges()) return (y_square < y_comp); 
				else return (y_comp < y_square);	
			}else{
				std::cout<<"!ERROR compareWave Di==NULL when comparing wave and sweepline"<<std::endl;
				exit(-1);
			}
		}
		if (sw1->useEdges()) return (y_square < y_comp); 
		else return (y_comp < y_square);	
	}
}

void Skeleton2DPoint::initializeEvents(){
	last_y=LLONG_MAX;
	events = new TypeEventQueuePoint();
	for (unsigned int id=0; id<polygon->size(); id++){
		Skeleton2DEventPoint se = Skeleton2DEventPoint(polygon->at(id));
		events->push(se);
	}
}

void Skeleton2DPoint::initializeWavefront(){
	bool (*fn_pt)(Skeleton2DWaveSP,Skeleton2DWaveSP) = compareWavePoint;
	wavefront = new TypeWavefront (fn_pt);
}

int Skeleton2DPoint::insertSpikeEvent(Skeleton2DWaveSP sw1, Skeleton2DWaveSP sw2, Skeleton2DEventPoint::Skeleton2DEventTypePoint type){
	if (sw1!=sw2){
		if (equalWaves(sw1,sw2)){
			#ifdef DEBUG
			std::cout<<"\t*Equal waves"<<std::endl;
			#endif
			graph->addEdge(sw1->getGraphId(), sw2->getGraphId());
			deleteWave(sw1);
			deleteWave(sw2);
			return -1;
		}else if (!isWavePairInserted(sw1,sw2)) {
			#ifdef DEBUG	
			std::cout<<"+Trying to insert spike event induced by "<<*sw1<<" and "<<*sw2<<std::endl;
			#endif
			Number Di[6];
			if (getSquare2Waves(sw1,sw2, Di)){
				if ((Di[1]<=polygon->getXmax())){
					Skeleton2DEventPoint se = Skeleton2DEventPoint(Di,sw1,sw2,type);
					events->push(se);
					#ifdef DEBUG	
					std::cout<<"+Spike event inserted at ("<<se.getX()<<","<<se.getY()<<")"<<std::endl;
					#endif
					return 1;
				}
			}else{
				#ifdef DEBUG	
				std::cout<<"-Spike event rejected"<<std::endl;
				#endif	
			}
		}
	}
	return 0;
}

Skeleton2DWaveSP Skeleton2DPoint::createNewWave(Skeleton2DEdge* se1, Skeleton2DEdge *se2, Number* DiConst, Skeleton2DGraph::VertexId graphId){
	Number Di[6];
	if (getSquare2EdgesLine(se1,se2,sweepline,Di)){
		Skeleton2DWaveSP swn (new Skeleton2DWave(se1,se2,graphId));
		return swn;
	}else {
		Skeleton2DWaveSP swn (new Skeleton2DWave(se1,se2,DiConst[5],DiConst[3],DiConst[2],graphId));
		return swn;
	}
}

bool Skeleton2DPoint::injectNextEvent(){
	if (!events->empty()){
		Skeleton2DEventPoint event = events->top();
		if (last_x!=event.getX()) {
			insertWaves(wavesToInsert);
			last_x = event.getX();
			last_y = event.getY();
			return true;
		}
		last_x= event.getX();
		last_y = event.getY();
		#ifdef DEBUG
		numProcessedEvents++;
		#endif
		events->pop();
		sweepline->setC(-event.getX());
		if (event.getType()==Skeleton2DEventPoint::SITE_EVENT){
			Skeleton2DVertex* sv = event.getVertex();
			Skeleton2DVertex* svn = polygon->getNextVertex(sv->id);
			Skeleton2DVertex* svp = polygon->getPrevVertex(sv->id); 
			sv->graphId = graph->addVertex(sv->v,0);
			bool upper=false, intersect=false, case14=false, case24=false;
			if (sv->v.x > svn->v.x){
				if (sv->v.y > svp->v.y ){
					#ifdef DEBUG
					std::cout<<"Case 1.2\n";	
					#endif
					intersect=true; upper=false;
				}else{
					#ifdef DEBUG
					std::cout<<"Case 2.3\n";	
					#endif
				}
			}else if (sv->v.x < svn->v.x){
				if (sv->v.y > svp->v.y ){
					#ifdef DEBUG
					std::cout<<"Case 1.4\n";	
					#endif
					intersect=true; case14=true; upper=true;
				}else{
					#ifdef DEBUG
					std::cout<<"Case 2.1\n";	
					#endif
				}
			}else if (sv->v.x > svp->v.x){
				if (sv->v.y > svn->v.y ){
					#ifdef DEBUG
					std::cout<<"Case 1.3\n";	
					#endif
				}else{
					#ifdef DEBUG
					std::cout<<"Case 2.2\n";	
					#endif
					intersect=true; upper=true;
				}
			}else{
				if (sv->v.y > svn->v.y ){
					#ifdef DEBUG
					std::cout<<"Case 1.1\n";	
					#endif
				}else{
					#ifdef DEBUG
					std::cout<<"Case 2.4\n";	
					#endif
					intersect=true; case24=true; upper=false;
				}
			}
			
			if (intersect){
				TypeWavefront::iterator itv;
				if (upper) {
					itv = getWavefrontUpperBound(sv->v.y, upper);
					if (itv!=wavefront->begin())
						--itv;
				}else{
					itv = getWavefrontLowerBound(sv->v.y, upper);
				}
				if (itv!=wavefront->end()){
					Skeleton2DWaveSP swint=(*itv);
					Skeleton2DEdge* se1 = polygon->getEdge(sv->id);
					Skeleton2DEdge* se2 = polygon->getPrevEdge(sv->id);
					#ifdef DEBUG
					std::cout<<"Wave ["<<*se1<<","<<*se2<<"] intersects "<<*swint<<std::endl;
					#endif
					if (case24||case14){
						Skeleton2DEdge* seint = getIntersectedEdge(swint,se1,se2,upper);
						Number Di[6];
						getSquare3Edges(se1,se2,seint,Di);
						if (seint!=NULL){
							Skeleton2DEventPoint::Skeleton2DEventTypePoint type;
							Skeleton2DWaveSP sw (new Skeleton2DWave(se1,se2,Di[5],Di[2],Di[3],sv->graphId));
							Skeleton2DWaveSP swn;
							if (upper){
								if (wavesShareEdge(sw, swint)){
									#ifdef DEBUG
									std::cout<<"SPIKE_FINAL_EVENT"<<std::endl;
									#endif
									type = Skeleton2DEventPoint::SPIKE_FINAL_EVENT;
									swn = swint;
								}else{
									#ifdef DEBUG
									std::cout<<"SPIKE_INITIAL_EVENT"<<std::endl;
									#endif
									type = Skeleton2DEventPoint::SPIKE_INITIAL_EVENT;
									swn = createNewWave(se1, seint,Di,sv->graphId);
									insertWave(swn);
								}
							}else {
								#ifdef DEBUG
								std::cout<<"SPIKE_INITIAL_EVENT"<<std::endl;
								#endif
								type = Skeleton2DEventPoint::SPIKE_INITIAL_EVENT;
								swn = createNewWave(se1, seint,Di,sv->graphId);
								insertWave(swn);
							}
							insertWave(sw);
							insertSpikeEvent(sw, swn, type);
						}else{
							std::cerr<<"Intersected element not found (seint==NULL)"<<std::endl;
							exit(-1);
						}
					}else{			
						if (equalWaveEdges(swint,se1,se2)){
							graph->addEdge(swint->getGraphId(), sv->graphId);
							deleteWave(swint);
						}else{
							Skeleton2DWaveSP sw(new Skeleton2DWave(se1,se2,sv->v.y,sv->v.y,sv->v.y,sv->graphId));
							insertSpikeEvent(sw, swint, Skeleton2DEventPoint::SPIKE_FINAL_EVENT);
							insertWave(sw);
						}
					}
				}else{
					std::cerr<<"Intersection not found"<<std::endl;
					exit(-1);
				}
			}else{
				Skeleton2DWaveSP sw(new Skeleton2DWave(polygon->getEdge(sv->id), polygon->getPrevEdge(sv->id), sv->graphId));
				wavesToInsert.push_back(sw);
			}
			#ifdef DEBUG
			numAcceptedEvents++;
			#endif
		}else{
			Skeleton2DWaveSP sw1 = event.getWave1();
			Skeleton2DWaveSP sw2 = event.getWave2();
			bool found1=false, found2=false;
			TypeWavefrontRange range1 = wavefront->equal_range(sw1);     
			for (TypeWavefront::iterator it=range1.first; it!=range1.second; ++it){
				if (*it==sw1) found1=true;
				if (*it==sw2) found2=true;
			}
			TypeWavefrontRange range2 = wavefront->equal_range(sw2);     
			for (TypeWavefront::iterator it=range2.first; it!=range2.second; ++it){
				if (*it==sw1) found1=true;
				if (*it==sw2) found2=true;
			}
			if (found1 && found2){
				//Spike event inducing waves must be neighbouring in the wavefront
				if (isInEqualRange(sw1,sw2)||isInEqualRange(sw2,sw1) || isUpperNeighbour(sw1,sw2) || isLowerNeighbour(sw1,sw2)){
					if (event.getSharedEdge()!=NULL){	
						Number* Di = event.getSquare();
						#ifdef DEBUG	
						std::cout<<"+Valid spike event at ("<<event.getX()<<","<<event.getY()<<") and waves "
						         <<*event.getWave1()<<" "<<*event.getWave2()<<" Type="<<event.getType()<<std::endl;
						#endif
						Skeleton2DGraph::VertexId vid;
						vid=graph->addConnectedVertex2Parent(event.getWave1()->getGraphId(),event.getWave2()->getGraphId(), Di[4], Di[5],Di[1]-Di[0]);	
						Skeleton2DWaveSP sn = createNewWave(event.getEdge1(), event.getEdge2(), Di ,vid);
						deleteWave(sw1);
						if (event.getType()==Skeleton2DEventPoint::SPIKE_INITIAL_EVENT){	
							if (event.getEdge2()->isHoritzontal())
								insertEqualNeighbourSpikeEvents(event.getWave2(),Skeleton2DEventPoint::SPIKE_FINAL_EVENT);
							else insertEqualNeighbourSpikeEvents(event.getWave2());
							insertUpperNeighbourSpikeEvents(event.getWave2());
							insertWave(sn);
							event.getWave2()->setGraphId(vid);	
							insertLowerNeighbourSpikeEvents(sn);
						}else {
							deleteWave(sw2);
							if (event.getType()==Skeleton2DEventPoint::SPIKE_FINAL_EVENT){
								insertWave(sn);
								event.getWave2()->setGraphId(vid);
								insertUpperNeighbourSpikeEvents(sn);								
							}else if (event.getType()==Skeleton2DEventPoint::SPIKE_NORMAL_EVENT){
								insertWave(sn);
								insertNeighbourWaveSpikeEvents(sn);
							}
						}
						#ifdef DEBUG
						numAcceptedEvents++;
						#endif
					}else{
						#ifdef DEBUG	
						std::cout<<"-Invalid spike event: no shared edges between spike event waves"<<std::endl;
						#endif
					}
				}else{
					#ifdef DEBUG		
					std::cout<<"-Invalid spike event: no neighbouring spike event waves"<<std::endl; 
					#endif
				}
			}else{
				#ifdef DEBUG	
				std::cout<<"-Invalid spike event: event wave "<<*sw1<<" and/or "<<*sw2<<" erased from wavefront"<<std::endl; 
				#endif
			}
		}
		#ifdef DEBUG
		printWavefrontInfo();
		updateWavefrontStatistics();
		#endif
		return true;
	}else return false;
}
