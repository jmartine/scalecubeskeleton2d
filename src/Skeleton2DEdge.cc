#include "Skeleton2DEdge.h"

Skeleton2DEdge::~Skeleton2DEdge(){
	delete sl;
}

Skeleton2DEdge::Skeleton2DEdge(int ID, Skeleton2DVertex* SV, Skeleton2DVertex* SVN) : id(ID), sv(SV), svn(SVN){
	//Set line coefficients and in direction
	if (sv->v.x==svn->v.x){
		//Vertical line
		sl = new Skeleton2DLine(1,0,-(sv->v.x),(svn->v.y < sv->v.y));
	}else if (sv->v.y==svn->v.y){ 
		//Horizontal line
		sl = new Skeleton2DLine(0,1,-(sv->v.y),(svn->v.x > sv->v.x));
	}
}

std::ostream& operator<<(std::ostream& out, const Skeleton2DEdge &se){
	return out <<se.getId();
}

Number Skeleton2DEdge::getMinY() const{
	if (sv->v.y < svn->v.y) return sv->v.y;
	else return svn->v.y;
}

bool Skeleton2DEdge::isHoritzontal(){
	return (sl->getA()==0);
}
