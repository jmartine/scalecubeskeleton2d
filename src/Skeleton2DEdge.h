#ifndef __SKELETON2DEDGE_H
#define __SKELETON2DEDGE_H

#include <iostream>
#include "Skeleton2DVertex.h"
#include "Skeleton2DLine.h"

class Skeleton2DEdge{
	private:
		int id;
		Skeleton2DVertex *sv, *svn;
		Skeleton2DLine* sl; 
		friend std::ostream& operator<<(std::ostream& out, const Skeleton2DEdge& se);
	public:
		~Skeleton2DEdge();
		
		//! \brief Constructor of an edge
		//! \param id internal id of the edge
		//! \param sv first vertex of the edge CCW
		//! \param svn second vertex of the edge CCW
		Skeleton2DEdge(int id, Skeleton2DVertex* sv, Skeleton2DVertex* svn);
		
		//! \return the first vertex of the edge CCW
		Skeleton2DVertex* getV() const {return sv;}
		
		//! \return the second vertex of the edge CCW
		Skeleton2DVertex* getVn() const {return svn;}
		
		//! \return the line that includes the edge
		Skeleton2DLine* getLine() const {return sl;}
		
		//! \return the edge internal identifier
		int getId() const {return id;}
		
		//! \return the minium y coordinate of the edge
		Number getMinY() const;
		
		//! \return true if the edge is horizontal and false otherwise
		bool isHoritzontal();
};

#endif 
