#include "Skeleton2DGraph.h"

using namespace std;

Number getInterpolation(const Number a1, const Number a2, const Number b1, const Number b2, const Number c2){
	if (a2==b2) return a2;
	else return (c2*(a1-b1)+b1*a2-b2*a1) / (a2-b2);
}

Skeleton2DGraph::Skeleton2DGraph(){
	vertexMap = boost::get(boost::vertex_color, graph);
	indexMap = boost::get(boost::vertex_index, graph);
}

Skeleton2DGraph::~Skeleton2DGraph(){
}

VertexInfo Skeleton2DGraph::getVertex(VertexId vid){
	return vertexMap[vid];
}

Skeleton2DGraph::VertexId Skeleton2DGraph::addConnectedVertex1Parent(VertexId vid, Number x, Number y, Number radius){
	VertexInfo vinfo = vertexMap[vid];
	Vertex v = vinfo.vertex;
	if ((v.x==x)&&(v.y==y)){
		return vid;
	}else{
		VertexId vidn = addVertex(x,y,radius);
		addEdge(vidn,vid);
		return vidn;
	}
}

Skeleton2DGraph::VertexId Skeleton2DGraph::addConnectedVertex2Parent(VertexId vid1,VertexId vid2, Number x, Number y, Number radius){
	VertexInfo vinfo1 = vertexMap[vid1];
	VertexInfo vinfo2 = vertexMap[vid2];
	Vertex v1 = vinfo1.vertex;
	Vertex v2 = vinfo2.vertex;
	if (((v1.x==x)&&(v1.y==y)) && ((v2.x==x)&&(v2.y==y))){
		return vid1;
	}else if ((v1.x==x)&&(v1.y==y)){
		addEdge(vid2, vid1);
		return vid1;
	}else if ((v2.x==x)&&(v2.y==y)){
		addEdge(vid1, vid2);
		return vid2;
	}else{
		VertexId vidn = addVertex(x,y,radius);
		addEdge(vidn,vid1);
		addEdge(vidn,vid2);
		return vidn;
	}
}

void Skeleton2DGraph::addEdge(VertexId id1, VertexId id2){
	if (id1!=id2){
		boost::add_edge(id1, id2, graph);
	}
}

void Skeleton2DGraph::removeEdge(VertexId id1, VertexId id2){
	boost::remove_edge(id1,id2,graph);
}

Skeleton2DGraph::VertexId Skeleton2DGraph::addVertex(Vertex v,Number radius){
	VertexInfo vinfo;
	vinfo.vertex=v;
	vinfo.radius=radius;
	return boost::add_vertex(vinfo,graph);
}

Skeleton2DGraph::VertexId Skeleton2DGraph::addVertex(Number x, Number y, Number radius){
	Vertex vn;
	vn.x = x;
	vn.y = y;
	return addVertex(vn,radius);
}

void Skeleton2DGraph::removeVertex(VertexId vid){
	boost::clear_vertex(vid, graph);
	boost::remove_vertex(vid, graph);
}

int Skeleton2DGraph::getVertexDegree(VertexId vid){
	return boost::degree(vid, graph);
}

int Skeleton2DGraph::getNumVertices(){
	return boost::num_vertices(graph);
}

int Skeleton2DGraph::getNumEdges(){
	return boost::num_edges(graph);
}

void Skeleton2DGraph::outputSvg(string path, Number xmax, Number ymax, bool close, double s) {
	ofstream  fileSvg;
	fileSvg.open(path.c_str());

	int stroke_width = 1 + max(xmax, ymax)/1000;
	fileSvg << "<?xml version=\"1.0\" standalone=\"no\"?>" << endl
			<< "<svg width=\""<< xmax << "\" height=\"" << ymax << "\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">" << endl;

	fileSvg << "<g id=\"cubeskeleton\">"<<std::endl;
	boost::graph_traits<Graph>::edge_iterator ei, ei_end;
	for (boost::tie(ei, ei_end) = boost::edges(graph); ei != ei_end; ++ei){
		VertexId vid1 = boost::source(*ei, graph);
		VertexId vid2 = boost::target(*ei, graph);
		Vertex p1 = vertexMap[vid1].vertex;
		Vertex p2 = vertexMap[vid2].vertex;

		fileSvg << "<line x1=\"" << p1.x/s << "\" y1=\"" << ymax - p1.y/s <<"\" x2=\"" << p2.x/s <<"\" y2=\"" << ymax - p2.y/s 
				<< "\" style=\"stroke:rgb(255,0,0);stroke-width:" << stroke_width << "\" />" << endl;		
	}
	fileSvg <<"</g>"<<std::endl;
	if(close)
		fileSvg << "</svg>" << endl;
	fileSvg.close();
}

void Skeleton2DGraph::addToRaster(Skeleton2DRaster* raster, double s) {	
	boost::graph_traits<Graph>::edge_iterator ei, ei_end;
	for (boost::tie(ei, ei_end) = boost::edges(graph); ei != ei_end; ++ei) {
		VertexId vid1 = boost::source(*ei, graph);
		VertexId vid2 = boost::target(*ei, graph);
		Vertex p1 = vertexMap[vid1].vertex;
		Vertex p2 = vertexMap[vid2].vertex;

		raster->addEdge(p1.x/s, p1.y/s, p2.x/s, p2.y/s, SKELETON);
	}	
}

void Skeleton2DGraph::saveBoxes(double scale, double shift){
	vector<double* > boxes;
	boost::graph_traits<Graph>::edge_iterator ei, ei_end;
	for (boost::tie(ei, ei_end) = boost::edges(graph); ei != ei_end; ++ei) {
		VertexId vid1 = boost::source(*ei, graph);
		VertexId vid2 = boost::target(*ei, graph);
		VertexInfo vi1 = vertexMap[vid1];
		VertexInfo vi2 = vertexMap[vid2];
		Vertex p1 = vi1.vertex;
		Vertex p2 = vi2.vertex;
		//double radius = vi1.radius*scale;
		Number radius = round(vi1.radius*scale);
		if (radius>0){
			double* box = NULL;
			if (p1.x==p2.x){
				box = new double[4];
				box[0]=p1.x - radius/2.0;
				box[1]=p1.x + radius/2.0;
				box[2]=(p1.y+p2.y - radius-abs(p1.y-p2.y))/2.0;
				box[3]=(p1.y+p2.y + radius+abs(p1.y-p2.y))/2.0;
			}else if (p1.y==p2.y){
				box = new double[4];
				box[0]=(p1.x+p2.x - radius-abs(p1.x-p2.x))/2.0;
				box[1]=(p1.x+p2.x + radius+abs(p1.x-p2.x))/2.0;
				box[2]=p1.y - radius/2.0;
				box[3]=p1.y + radius/2.0;			
			}
			if  (box!=NULL){
				for (unsigned int i=0; i<4; i++){
					box[i]=box[i]/shift;
				}
				boxes.push_back(box);
			}
		}
	}
	pair<vertex_iter, vertex_iter> vp;
	for (vp = vertices(graph); vp.first != vp.second; ++vp.first){
		VertexInfo vi = vertexMap[*vp.first];
		//double radius = vi.radius*scale;
		Number radius = round(vi.radius*scale);
		if (radius>0)
			addBoxExtremeFormat(boxes, vi.vertex, radius, shift);	
	}
	ofstream file;
	file.open ("boxes.txt", ofstream::out);
	file.precision(numeric_limits< Number >::digits10);
	for (vector<double* >::iterator itb=boxes.begin(); itb!=boxes.end(); ++itb){
		double* box = *itb;
		file<<box[0]<<" "<<box[1]<<" "<<box[2]<<" "<<box[3]<<endl;
		delete[] box;
	}
	file.close();
}

void Skeleton2DGraph::addBoxExtremeFormat(vector<double* >& boxes, Vertex& v, Number radius, double div) {
	double* box = new double[4];
	radius /= 2;
	box[0]=v.x - radius;
	box[1]=v.x + radius;
	box[2]=v.y - radius;
	box[3]=v.y + radius;
	for (unsigned int i=0; i<4; i++) 
		box[i]=box[i]/div;
	boxes.push_back(box);		
}

void Skeleton2DGraph::insertVertexCoord(Number x, Number y, VertexId vid){
	pair<TypeMapVertexCoordPair::iterator, bool> ret;
	pair<Number ,Number> vpair (x,y);
	set<VertexId> setid;
	setid.insert(vid);
	pair<pair<Number,Number>,set<VertexId> > insertInfo (vpair, setid);
	ret=mapVertexCoordPair.insert(insertInfo); 
	if (ret.second==false){
		//already exists a vertex in the same position
		ret.first->second.insert(vid);
		setid.clear();
	}
}

bool alignedPairVertices(const Vertex& v1, const Vertex& v2){
	Number dx = fabs(v1.x - v2.x);
	Number dy = fabs(v1.y - v2.y);
	return dx==dy;
}

bool alignedVertices(Vertex v1, Vertex v2, Vertex v3){
	if (alignedPairVertices(v1,v3)&& alignedPairVertices(v2,v3)&&alignedPairVertices(v1,v2)){
		Number minx = min(v2.x,v3.x);
		Number maxx = max(v2.x,v3.x);
		Number miny = min(v2.y,v3.y);
		Number maxy = max(v2.y,v3.y);
		return ((v1.x>=minx) && (v1.x<=maxx) && (v1.y>=miny) && (v1.y<=maxy));
	}else return false;
}

void Skeleton2DGraph::repairCollinearities(bool edgeMode){
	pair<vertex_iter, vertex_iter> vp;
	vector<VertexId> verticesToRepair,verticesToRepair2;
	for (vp = vertices(graph); vp.first != vp.second; ++vp.first){
		VertexId vid = *vp.first;
		if (getVertexDegree(vid)==3) verticesToRepair.push_back(vid);
	}
	unsigned int numRepaired=0;
	for (vector<VertexId>::reverse_iterator it = verticesToRepair.rbegin(); it!=verticesToRepair.rend(); ++it)		
		if (!repairType(*it,edgeMode, false,numRepaired)) verticesToRepair2.push_back(*it);
	verticesToRepair.clear();
	for (vector<VertexId>::reverse_iterator it = verticesToRepair2.rbegin(); it!=verticesToRepair2.rend(); ++it)
		repairType(*it,edgeMode, true,numRepaired);
	verticesToRepair2.clear();
}

bool Skeleton2DGraph::repairType(VertexId vid, bool edgeMode, bool normalIteration, unsigned int& numRepaired){
	boost::graph_traits<Graph>::adjacency_iterator ai1, ai_end1, ai2, ai_end2, aitemp, ai_endtemp;
	for (boost::tie(ai1, ai_end1) = adjacent_vertices(vid, graph);ai1 != ai_end1; ++ai1){
		VertexId vidn1=*ai1;
		for (boost::tie(ai2, ai_end2) = adjacent_vertices(vidn1, graph); ai2 != ai_end2; ++ai2){
			VertexId vidn2= *ai2;
			VertexInfo vinfo  = vertexMap[vid]; 
			VertexInfo vninfo1 = vertexMap[vidn1];
			VertexInfo vninfo2 = vertexMap[vidn2];
			Vertex v = vinfo.vertex;
			Vertex vn1 = vninfo1.vertex;
			Vertex vn2 = vninfo2.vertex;
			if ((vid!=vidn2) && (alignedVertices(v,vn1,vn2))){	
				//get direction
				bool up, right, vertical, found = false;
				up = ( v.y > vn1.y);
				right = ( v.x > vn1.x);
				for (boost::tie(aitemp, ai_endtemp) = adjacent_vertices(vid, graph);aitemp != ai_endtemp; ++aitemp){
					VertexId vidtemp=*aitemp;
					if ((vidtemp!=vidn1)&&(vidtemp!=vidn2)){
						VertexInfo vtempinfo = vertexMap[vidtemp];
						Vertex vtemp = vtempinfo.vertex;
						if (vtemp.x==vn2.x) {
							vertical=true;
							found=true;
							break;
						}else if (vtemp.y==vn2.y) {
							vertical=false;
							found=true;
							break;
						}
					}
				}
				if (!found) return true;
				bool repair = false;
				if (!vertical && !normalIteration) repair =true;
				else if (!edgeMode && normalIteration) repair= true;
				else if (edgeMode && vertical) repair = !(up xor normalIteration);
				if (repair) {
					numRepaired++;
					removeEdge(vidn1,vidn2);
					removeEdge(vid,vidn1);
					addEdge(vid,vidn2);	
					VertexId lastInnerVid=vidn1, prevLastInnerVid;
					Vertex lastInner=vn1, prevLastInner;
					bool first=true, cond;
					do{
						prevLastInner=lastInner;
						prevLastInnerVid=lastInnerVid;
						lastInnerVid=getInnerLastVertex(lastInnerVid,vertical,up,right,first);
						first=false;
						if (lastInnerVid==NULL) return false;	 
						VertexInfo lastInnerInfo = vertexMap[lastInnerVid];
						lastInner=lastInnerInfo.vertex;
						if (vertical) cond = ( ((lastInner.y > v.y)&& !up) || ((lastInner.y < v.y)&& up) );
						else cond = ( ((lastInner.x < v.x) && right) || ((lastInner.x > v.x) && !right) );
					}while (cond);
					VertexId vidnew=NULL;
					VertexInfo lastInnerInfo = vertexMap[lastInnerVid];
					VertexInfo prevLastInnerInfo = vertexMap[prevLastInnerVid];
					if (vertical){
						if (lastInner.y==v.y) addEdge(lastInnerVid,vid);
						else {
							Number xinterp = getInterpolation(lastInner.x,lastInner.y,prevLastInner.x,prevLastInner.y,v.y);
							vidnew = addVertex(xinterp, v.y, getInterpolation(lastInnerInfo.radius,lastInner.y,prevLastInnerInfo.radius,prevLastInner.y,v.y));//lastInnerInfo.radius);
						}
					}else{
						if (lastInner.x==v.x) addEdge(lastInnerVid,vid);
						else {
							Number yinterp = getInterpolation(lastInner.y,lastInner.x,prevLastInner.y,prevLastInner.x,v.x);
							vidnew = addVertex(v.x, yinterp, getInterpolation(lastInnerInfo.radius,lastInner.x,prevLastInnerInfo.radius,prevLastInner.x,v.x));//lastInnerInfo.radius);
						}
					}
					if (vidnew!=NULL){
						addEdge(vidnew,prevLastInnerVid);
						addEdge(vidnew,lastInnerVid);
						addEdge(vidnew,vid);
						removeEdge(prevLastInnerVid,lastInnerVid);
					}
					if (getVertexDegree(vidn1)==2){
						VertexId nvids[2]; int n=0;
						for (boost::tie(aitemp, ai_endtemp) = adjacent_vertices(vidn1, graph);aitemp != ai_endtemp; ++aitemp){
							nvids[n]=*aitemp;n++;
						}
						addEdge(nvids[0],nvids[1]);
						removeVertex(vidn1);
					}
					return true;
				}
			}
		}
	}
	return false;
}

Skeleton2DGraph::VertexId Skeleton2DGraph::getInnerLastVertex(VertexId vid, bool vertical, bool up, bool right, bool first){
	boost::graph_traits<Graph>::adjacency_iterator ai, ai_end;
	VertexId innerVid=NULL;
	VertexInfo vinfo = vertexMap[vid];
	Vertex v = vinfo.vertex;
	int maxPriority=INT_MIN, priority;
	for (boost::tie(ai, ai_end) = adjacent_vertices(vid, graph);ai != ai_end; ++ai){
		VertexId vidn= *ai;
		if (vid!=vidn){
			VertexInfo vninfo= vertexMap[vidn];
			Vertex vn = vninfo.vertex;
			bool cont=false;
			if (vertical) cont = (( (vn.y > v.y) && up) || ((vn.y < v.y) && !up));
			else cont = ( ((vn.x > v.x) && right) || ((vn.x < v.x) && !right));
			if (cont){
				bool returnVidn  = false;
				if (vertical) returnVidn = ((!first && ((vn.x > v.x) && right)) || ((vn.x < v.x) && !right));
				else returnVidn = ((!first && ((vn.y > v.y) && up)) || ((vn.y < v.y) && !up));
				if (returnVidn) return vidn;
				//assign priority
				priority=-1;
				if (vertical){
					if (vn.x == v.x) priority = 1;
					else if ((vn.x < v.x) && right) priority = 0;
					else if ((vn.x > v.x) && !right) priority = 0;
				}else{
					if (vn.y == v.y) priority = 1;
					else if ((vn.y < v.y) && up) priority = 0;
					else if ((vn.y > v.y) && !up) priority = 0;
				}
				if ((priority>maxPriority)&&(priority!=-1)){
					innerVid=vidn;
					maxPriority=priority;
				}					
			}
		}
	}
	return innerVid;
}

void Skeleton2DGraph::repairCoincidences(){
	pair<vertex_iter, vertex_iter> vp;
	for (vp = vertices(graph); vp.first != vp.second; ++vp.first){
		VertexId vid = *vp.first;
		VertexInfo vinfo = vertexMap[vid];
		Vertex v = vinfo.vertex;
		insertVertexCoord(v.x, v.y, vid);
	}
	for (TypeMapVertexCoordPair::iterator it= mapVertexCoordPair.begin(); it!=mapVertexCoordPair.end(); ++it){
		set<VertexId> setid = (*it).second;
		pair<Number, Number> vpair = (*it).first;
		if (setid.size()>1){
			//collect all the neighbours
			set<VertexId> neighbours;
			for (set<VertexId>::iterator itvid=setid.begin(); itvid!=setid.end(); ++itvid){
				VertexId vid = *itvid;
				boost::graph_traits<Graph>::adjacency_iterator ai, ai_end;
				for (boost::tie(ai, ai_end) = adjacent_vertices(vid, graph);ai != ai_end; ++ai){
					VertexId vidn= *ai;
					if (setid.find(vidn)==setid.end()){
						neighbours.insert(*ai);
					}
				}
			}
			Number radius=0;
			for (set<VertexId>::iterator itvid=setid.begin(); itvid!=setid.end(); ++itvid){
				VertexId vid=*itvid;
				VertexInfo vinfo = vertexMap[vid];
				if (vinfo.radius>radius) radius = vinfo.radius;
			}
			//erase all the vertexs
			for (set<VertexId>::iterator itvid=setid.begin(); itvid!=setid.end(); ++itvid){
				VertexId vid=*itvid;
				removeVertex(vid);
			}
			//create a new vertex and connect with neighbours
			VertexId vidn = addVertex(vpair.first,vpair.second,radius);
			for (set<VertexId>::iterator itvid=neighbours.begin(); itvid!=neighbours.end(); ++itvid){
				VertexId vid=*itvid;
				addEdge(vidn, vid);
			}
		}
	}
	mapVertexCoordPair.clear();
}

void Skeleton2DGraph::pruneGraph(int degree){
	pair<vertex_iter, vertex_iter> vp;
	vector<VertexId> verticesToRemove;
	for (vp = vertices(graph); vp.first != vp.second; ++vp.first){
		VertexId vid = *vp.first;
		if (getVertexDegree(vid)<=degree) verticesToRemove.push_back(vid);
	}
	for (vector<VertexId>::iterator it= verticesToRemove.begin(); it!=verticesToRemove.end(); ++it){
		removeVertex((*it));
	}
}

#ifdef DEBUG
void Skeleton2DGraph::printInfo(){
	cout<<"Graph Information\n\t#Vertices="<<getNumVertices()<<"\n\t#Edges="<<getNumEdges()<<endl;
}
#endif
