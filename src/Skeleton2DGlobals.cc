#include "Skeleton2DGlobals.h"

double getSeconds(const timeval& iniTime, const timeval& endTime){
	double iniTimeM = iniTime.tv_sec * 1000000 + iniTime.tv_usec;
	double  endTimeM = endTime.tv_sec * 1000000 + endTime.tv_usec;
	return (endTimeM - iniTimeM) / 1000000;
}

bool operator<(const Vertex& v, const Vertex& u) {
     return (v.x<u.x || (v.x==u.x && v.y<u.y));
}
