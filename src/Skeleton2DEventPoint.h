#ifndef __SKELETON2DEVENTPOINT_H
#define __SKELETON2DEVENTPOINT_H

#include "Skeleton2DVertex.h"
#include "Skeleton2DEvent.h"

class Skeleton2DEventPoint : public Skeleton2DEvent{
	public:
		enum Skeleton2DEventTypePoint {SITE_EVENT, SPIKE_NORMAL_EVENT, SPIKE_INITIAL_EVENT, SPIKE_FINAL_EVENT};
		
		//! \return the type of event
		enum Skeleton2DEventTypePoint getType() const {return type;}
		
		//! \brief Constructor of site event
		//! \param sv Vertex of the input orthogonal polygon
		Skeleton2DEventPoint(Skeleton2DVertex* sv);
		
		//! \brief Constructor of a spike event
		//! \param Di Event square
		//! \param sw1 First inducing wave
		//! \param sw2 Second inducing wave
		//! \param type Type of spike event (SPIKE_NORMAL_EVENT, SPIKE_INITIAL_EVENT, SPIKE_FINAL_EVENT)
		Skeleton2DEventPoint(Number* Di, Skeleton2DWaveSP const& sw1, Skeleton2DWaveSP const& sw2, enum Skeleton2DEventTypePoint type=SPIKE_NORMAL_EVENT);
		
		//! \return if SITE_EVENT, returns the associated polygon vertex
		Skeleton2DVertex* getVertex() const {return sv;}
		
		//! \return if SPIKE_EVENT, returns the y coordinate priority
		Number getY() const {return y;}
		
		//! \return if SPIKE_EVENT, returns the y origin coordinate priority
		Number getYOrigen() const {return yOrigen;}
		
		//! \return true if is origin SPIKE_EVENT
		bool isOrigen() const { return origen;}
		
		//! \return true if is SITE_EVENT
		bool isSite() const { return getType()==Skeleton2DEventPoint::SITE_EVENT;}
	private:
		enum Skeleton2DEventTypePoint type;
		Skeleton2DVertex* sv; 
		Number y,yOrigen;
		bool origen;
};

class Skeleton2DEventComparePoint{
    public:
		bool operator()(Skeleton2DEventPoint& se1, Skeleton2DEventPoint& se2);
};

#endif
