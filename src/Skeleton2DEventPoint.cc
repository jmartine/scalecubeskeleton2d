#include "Skeleton2DEventPoint.h"

Skeleton2DEventPoint::Skeleton2DEventPoint(Skeleton2DVertex* SV) : type(SITE_EVENT), sv(SV){
	x=SV->v.x;
	y=SV->v.y;
	yOrigen=origen=DBL_MAX;
}

Skeleton2DEventPoint::Skeleton2DEventPoint(Number* DI, Skeleton2DWaveSP  const& SW1, Skeleton2DWaveSP const& SW2, enum Skeleton2DEventTypePoint TYPE) : type(TYPE){
	x=DI[1];
	y=DI[2];
	sw1=SW1;
	sw2=SW2;
	for (int i=0; i<6; i++) Di[i]=DI[i];
	classifyEdges(sw1,sw2, sharedEdge,edge1,edge2);
	origen=getOrigen(sharedEdge,edge1,edge2, yOrigen);
}

bool Skeleton2DEventComparePoint::operator()(Skeleton2DEventPoint& se1, Skeleton2DEventPoint& se2){
	if (se1.getX()==se2.getX()){
		if (se1.getY()==se2.getY()) {
			if ((!se1.isSite() && se1.isOrigen())&&
			    (!se2.isSite() && se2.isOrigen())){
				return se1.getYOrigen()<se2.getYOrigen();
			}else if ((!se1.isSite() && !se1.isOrigen())&&
			         (!se2.isSite() && se2.isOrigen())){
				return false;
			}else if ((!se1.isSite() && se1.isOrigen())&&
			         (!se2.isSite() && !se2.isOrigen())){
				return true;
			}else{					
				return (!se2.isSite());
			}
		}else return se1.getY()>se2.getY(); 
	}else return (se1.getX()>se2.getX());
}
