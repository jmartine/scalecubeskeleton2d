#include "Skeleton2DSegment.h"

using namespace std;

extern Skeleton2DLine* sweepline;

Skeleton2DSegment::~Skeleton2DSegment(){
	delete events;
}

//sw1 < sw2  sw1 in earlier position than sw2
bool compareWaveSegment(Skeleton2DWaveSP sw1, Skeleton2DWaveSP sw2) {
	if ((!sw1->useEdges())&&(!sw2->useEdges()) ){
		cout<<"!ERROR compareWave Unable to compare two points (wave required)"<<endl;
		exit(-1);
	}
	if (sw1->useEdges() && sw2->useEdges()){
		Number Di[6];
		getSquareWaveLine(sw1,sweepline,Di);
		Number cy1 = Di[5];
		getSquareWaveLine(sw2,sweepline,Di);
		Number cy2 = Di[5];
		return (cy1 < cy2);
	}else{
		Skeleton2DWaveSP sw;
		Number y_comp;
		bool lowerOrdinate;
		if (sw1->useEdges()){
			sw = sw1;
			y_comp = sw2->getY();
			lowerOrdinate= sw2->isLowerOrdinate();
		}else{
			sw = sw2;
			y_comp = sw1->getY();
			lowerOrdinate= sw1->isLowerOrdinate();
		}
		Number Di[6];
		if (getSquareWaveLine(sw,sweepline,Di)){
			Number y_square;
			if (lowerOrdinate) y_square = Di[2];
			else y_square = Di[3];
			if (sw1->useEdges()) return (y_square < y_comp); 
			else return (y_comp < y_square);	
		}else{
			cout<<"!ERROR compareWave Di==NULL when comparing wave and sweepline"<<endl;
			exit(-1);
		}
	}
}

void Skeleton2DSegment::initializeEvents(){
	events = new TypeEventQueueSegment();
	numSiteEvents=0;
	for (unsigned int id=0; id<polygon->size(); id++){
		Skeleton2DVertex* sv = polygon->at(id);
		Skeleton2DVertex* svn = polygon->getNextVertex(id);
		if (sv->v.x==svn->v.x){
			Skeleton2DEventSegment se = Skeleton2DEventSegment(sv->v.x, polygon->getEdge(id));
			events->push(se);
			numSiteEvents++;
		}
	}
}

void Skeleton2DSegment::initializeWavefront(){
	bool (*fn_pt)(Skeleton2DWaveSP,Skeleton2DWaveSP) = compareWaveSegment; // function pointer as Compare
	wavefront = new TypeWavefront (fn_pt);
}

void Skeleton2DSegment::getIntersection(Skeleton2DWaveSP const& sw, Skeleton2DEdge* siteEdge, Skeleton2DEdge* noSiteEdge, Skeleton2DVertex* sv, bool upper){
	Skeleton2DEdge* sharedEdge = getIntersectedEdge(sw,siteEdge,noSiteEdge,upper);
	Skeleton2DEdge *sr1, *sr2;
	if (sharedEdge==NULL){
		cerr<<"!ERROR getIntersection() Case not supported"<<endl;
        exit(-1);	
	}
	Number Di[6],x,y;
	getSquare3Edges(siteEdge, noSiteEdge, sharedEdge,Di);
	x=Di[4]; y=Di[5];
	sr1=noSiteEdge;
	sr2=sharedEdge;
	#ifdef DEBUG
	cout<<"+Intersection at ("<<x<<","<<y<<")  Outgoing wave=["<<*sr1<<","<<*sr2<<"]"<<endl;
	#endif
	Skeleton2DGraph::VertexId vid = graph->addConnectedVertex1Parent(sv->graphId,x,y,Di[1]-Di[0]);
	sv->graphId=vid;
	Skeleton2DWaveSP swn(new Skeleton2DWave(sr1,sr2,vid));
	wavesToInsertImmediate.push_back(swn);
}

int Skeleton2DSegment::insertSpikeEvent(Skeleton2DWaveSP sw1, Skeleton2DWaveSP sw2, Skeleton2DEventPoint::Skeleton2DEventTypePoint type){
	if (!isWavePairInserted(sw1,sw2)) {
		#ifdef DEBUG	
		cout<<"+Trying to insert spike event induced by "<<*sw1<<" and "<<*sw2<<endl;
		#endif
		Number Di[6];
		if (getSquare2Waves(sw1,sw2,Di)){
			if (Di[1]<=polygon->getXmax()){
				Skeleton2DEventSegment se = Skeleton2DEventSegment(Di,sw1,sw2);
				events->push(se);
				#ifdef DEBUG	
				cout<<"+Spike event inserted at t="<<se.getX()<<endl;
				#endif
			}
			return 1;
		}else{
			#ifdef DEBUG	
			cout<<"-Spike event rejected"<<endl;
			#endif
		}
	}
	return 0;
}

bool Skeleton2DSegment::injectNextEvent(){
	if (!events->empty()){
		Skeleton2DEventSegment event = events->top();
		if (last_x!=event.getX()) {
			insertWaves(wavesToInsert);
			last_x = event.getX();
			return true;
		}else last_x= event.getX();
		events->pop();
		#ifdef DEBUG
		numProcessedEvents++;
		#endif
		sweepline->setC(-event.getX());
		if (event.getType()==Skeleton2DEventSegment::SITE_EVENT){
			numSiteEvents--;
			// 1. Determine the portion of wavefront that becomes part of the Voronoi diagram
			#ifdef DEBUG	
			cout<<endl<<event<<endl;
			cout<<"1. Determine v1,v2"<<endl; 
			#endif
			// p1 is assumed to be above p2 and wavefront between v1 and v2 is finalized
			Skeleton2DEdge* se = event.getEdge();
			Skeleton2DVertex* sv = se->getV();   //first edge vertex
			Skeleton2DVertex* svn = se->getVn(); //next vertex CCW
			Skeleton2DVertex* svp = polygon->getPrevVertex(sv->id); //previous vertex wrt v
			Skeleton2DVertex* svc = polygon->getNextVertex(svn->id); //next vertex wrt vn
			sv->graphId = graph->addVertex(sv->v,0);
			svn->graphId = graph->addVertex(svn->v,0);
			Skeleton2DVertex *p1, *p2;
			bool insertWavefront=false;
			bool case24=false, case14=false;
			if (sv->v.y > svn->v.y){
				//v = p1 | vn = p2
				p1 = sv; p2 = svn;
				Skeleton2DWaveSP sw1 (new Skeleton2DWave(polygon->getPrevEdge(se->getId()),se,sv->graphId));
				wavesToInsert.push_back(sw1);
				Skeleton2DWaveSP sw2 (new Skeleton2DWave(se, polygon->getNextEdge(se->getId()),svn->graphId));
				wavesToInsert.push_back(sw2);
				#ifdef DEBUG	
				if (sv->v.x > svp->v.x){ cout<<"1.3\n";
				}else{cout<<"1.1\n"; }
				if (svn->v.x > svc->v.x){cout<<"2.3\n";
				}else{cout<<"2.1\n";}
				#endif
			}else{
				//v = p2 | vn = p1
				insertWavefront=true;
				p2 = sv; p1 = svn;
				if (sv->v.x > svp->v.x){
					#ifdef DEBUG	
					cout<<"Case 2.2\n";	
					#endif
				}else{
					#ifdef DEBUG	
					cout<<"Case 2.4\n";
					#endif
					case24=true;
				}
				if (svn->v.x > svc->v.x){
					#ifdef DEBUG	
					cout<<"Case 1.2\n"; 
					#endif
				}else{
					#ifdef DEBUG	
					cout<<"Case 1.4\n";
					#endif
					case14=true;
				}
			}		
			if (insertWavefront){
				TypeWavefront::iterator itv1 = getWavefrontUpperBound(p1->v.y, true);
				if (itv1!=wavefront->begin())
					 --itv1;
				TypeWavefront::iterator itv2 = getWavefrontLowerBound(p2->v.y, false);
				if ((itv2!=wavefront->end())){
					Skeleton2DWaveSP sw1 = *itv1, sw2 = *itv2;
					if (case24) getIntersection(sw2,se,polygon->getPrevEdge(se->getId()),p2, false);
					if (case14) getIntersection(sw1,se,polygon->getNextEdge(se->getId()),p1, true);
					#ifdef DEBUG	
					cout<<"2.Insert the portion of wavefront between v2="<<*(*itv2);
					if (itv1!=wavefront->end()) cout<<" and v1="<<*(*itv1)<<endl;
					else  cout<<" and the end"<<endl;
					#endif
					if (itv1!=wavefront->end()) ++itv1;
					TypeWavefront::iterator it=itv2;
					vector<Skeleton2DWaveSP> erWaves, erWaves1, erWaves2;
					Number lastDi5=0;
					if ((itv1==itv2)&&(case14||case24)) {
						if (wavesShareEdge(sw1,sw2)) graph->addEdge(p1->graphId,p2->graphId);
					}else while (it!=itv1){
						Skeleton2DWaveSP sw = *it;			
						Number Di[6];
						getSquareWaveLine(sw,sweepline,Di);
						Skeleton2DGraph::VertexId vid = graph->addConnectedVertex1Parent(sw->getGraphId(), Di[4],Di[5], Di[1]-Di[0]);
						sw->setGraphId(vid);
						if (it==itv2) graph->addEdge(vid,p2->graphId);
						if (it==itv2 || lastDi5==Di[5]) {
							erWaves.push_back(sw);
						}else{
							erWaves2.clear(); erWaves2=erWaves1;
							erWaves1.clear(); erWaves1=erWaves;
							erWaves.clear();  erWaves.push_back(sw);
							for (vector<Skeleton2DWaveSP>::iterator it1=erWaves.begin(); it1!=erWaves.end() ;++it1){
								for (vector<Skeleton2DWaveSP>::iterator it2=erWaves1.begin(); it2!=erWaves1.end();++it2){
									if (wavesShareEdge(*it1,*it2)) {
										graph->addEdge((*it2)->getGraphId(),(*it1)->getGraphId());
									}
								}
							}
							for (vector<Skeleton2DWaveSP>::iterator it1=erWaves1.begin(); it1!=erWaves1.end();++it1){
								for (vector<Skeleton2DWaveSP>::iterator it2=erWaves2.begin(); it2!=erWaves2.end();++it2){
									if (wavesShareEdge(*it1,*it2)){
										graph->addEdge((*it2)->getGraphId(),(*it1)->getGraphId());
									}
								}
							}	
						}
						lastDi5=Di[5];
						++it;
						if (it==itv1) {
							graph->addEdge(vid,p1->graphId);
							sw->setGraphId(vid);
						}	
					}
					// 3. Delete from the sweep line status all waves between v1 and v2 (except those containing v1 and v2)
					#ifdef DEBUG	
					cout<<"3. Delete waves between v1,v2"<<endl;
					#endif
					wavefront->erase(itv2,itv1);
					insertWaves(wavesToInsertImmediate);
				}
			}
			#ifdef DEBUG
			numAcceptedEvents++;	
			#endif
		}else if (event.getType()==Skeleton2DEventSegment::SPIKE_EVENT){
			if (numSiteEvents>0){	
				Skeleton2DWaveSP sw1 = event.getWave1();
				Skeleton2DWaveSP sw2 = event.getWave2();	
				#ifdef DEBUG
				cout<<"SPIKE EVENT at x="<<event.getX()<<endl;		
				#endif
				bool found1=false, found2=false;
				TypeWavefrontRange range1 = wavefront->equal_range(sw1);  
				for (TypeWavefront::iterator it=range1.first; it!=range1.second; ++it){
					if (*it==sw1) found1=true;
					if (*it==sw2) found2=true;
				}
				TypeWavefrontRange range2 = wavefront->equal_range(sw2);     
				for (TypeWavefront::iterator it=range2.first; it!=range2.second; ++it){
					if (*it==sw1) found1=true;
					if (*it==sw2) found2=true;
				}
				if (found1 && found2){
					#ifdef DEBUG
					cout<<endl<<event<<endl;
					#endif
					//Spike event inducing waves must be neighbouring in the wavefront
					if (isInEqualRange(sw1,sw2)||isInEqualRange(sw2,sw1) || isUpperNeighbour(sw1,sw2) || isLowerNeighbour(sw1,sw2)){
						if (event.getSharedEdge()!=NULL){	
							Number* Di = event.getSquare();
							#ifdef DEBUG	
							cout<<"+Valid spike event Di["<<Di[0]<<","<<Di[1]<<","<<Di[2]<<","<<Di[3]<<"]"<<endl;
							#endif
							Skeleton2DGraph::VertexId vid;
							vid=graph->addConnectedVertex2Parent(event.getWave1()->getGraphId(),event.getWave2()->getGraphId(), Di[4], Di[5],Di[1]-Di[0]);
							event.getSharedEdge()->getV()->graphId=	event.getSharedEdge()->getVn()->graphId = vid;
							Skeleton2DWaveSP sn(new Skeleton2DWave(event.getEdge1(), event.getEdge2(),vid));
							deleteWave(sw1); deleteWave(sw2);
							insertWave(sn);
							#ifdef DEBUG	
							cout<<"+Generate neighbour spike events"<<endl;
							numAcceptedEvents++;
							#endif
							insertNeighbourWaveSpikeEvents(sn);
						}else{
							#ifdef DEBUG	
							cout<<"-Invalid spike event: no shared edges between spike event waves"<<endl;
							#endif
						}
					}else{
						#ifdef DEBUG		
						cout<<"-Invalid spike event: no neighbouring spike event waves"<<endl; 
						#endif
					}
				}else{
					#ifdef DEBUG	
					cout<<"-Invalid spike event: event wave "<<*sw1<<" and/or "<<*sw2<<" erased from wavefront"<<endl; 
					#endif
				}
			}
		}
		#ifdef DEBUG
		printWavefrontInfo();
		updateWavefrontStatistics();
		#endif
		return true;
	}else return false;
}
