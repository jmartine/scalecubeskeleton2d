#include "Skeleton2D.h"

Skeleton2DLine* sweepline=NULL;

void Skeleton2D::initialize(){
	#ifdef DEBUG
	this->numProcessedEvents=this->numAcceptedEvents=this->maxWavefrontSize=this->sumWavefrontSizes=0;
	#endif
	initializeWavefront();
	if (polygon->size()<4) {
		std::cerr<<"!ERROR Skeleton2D() Not enough vertices (v<4)"<<std::endl;		
		this->valid=false;
		return;
	}
	if (!polygon->isOrthogonal()){
		std::cerr<<"!ERROR Skeleton2D() Polygon not orthogonal"<<std::endl;
		delete polygon;
		this->valid=false;
		return;
	}
	timeval iniTime, endTime;
	(void) gettimeofday(&iniTime, NULL);
	initializeEvents();
	sweepline = new Skeleton2DLine(1,0,0,false);
	std::cout<<"* Computing L infinity Voronoi diagram... "<<std::flush;
	while (injectNextEvent());
	(void) gettimeofday(&endTime, NULL);
	std::cout<<getSeconds(iniTime, endTime)<<"s."<<std::endl;
	#ifdef DEBUG
	printInfo();
	#endif
	if (valid){
		graph->repairCoincidences();
		Number shift = polygon->getShift();
		graph->saveBoxes(params.multDistScale, shift);
		if (params.cubeskeleton) {
			cout<<"* Extracting interior cube skeleton... "<<std::flush;
			(void) gettimeofday(&iniTime, NULL);
			graph->repairCollinearities(!params.pointMode);
			pruneGraph();
			(void) gettimeofday(&endTime, NULL);
			cout<<getSeconds(iniTime, endTime)<<"s."<<endl;
		}
		int xmax, ymax;
		if(params.boundx > 0) {
			xmax = params.boundx;
			ymax = params.boundy;
		}else {
			xmax = polygon->getXmax()/shift;
			ymax = polygon->getYmax()/shift;
		}
		if(params.svgOutput != "") {
			graph->outputSvg(params.svgOutput, xmax, ymax, !params.outputBorders, shift);
			if(params.outputBorders) 
				polygon->appendToSvg(params.svgOutput, xmax, ymax);
		}
		if(params.ppmOutput != "") {			
			Skeleton2DRaster raster (polygon->getXmin(), polygon->getYmin(), xmax, ymax);
			graph->addToRaster(&raster, shift);
			if(params.outputBorders) 
				polygon->addToRaster(&raster);
			raster.exportRaster(params.ppmOutput);
		}
	}
}

Skeleton2D::~Skeleton2D(){
	delete polygon;
	delete sweepline;
	delete wavefront;
	delete graph;
}

Skeleton2D::Skeleton2D(Skeleton2DParams& PARAMS) : wavefront(NULL), params(PARAMS), valid(true), last_x(LLONG_MAX){
	polygon = new Skeleton2DPolygon(params.filename);
	graph = new Skeleton2DGraph();
}

Skeleton2D::TypeWavefront::iterator Skeleton2D::findWave(Skeleton2DWaveSP const& sw){
	TypeWavefrontRange range = wavefront->equal_range(sw);
	for (TypeWavefront::iterator it=range.first; it!=range.second; ++it){
		#ifdef DEBUG
		std::cout<<"Equal range="<<*(*it)<<std::endl;
		#endif
		if ((*it)==sw) return it;
	}
	return wavefront->end();
}

void Skeleton2D::deleteWave(Skeleton2DWaveSP const& sw){
	#ifdef DEBUG
	std::cout<<"Delete wave "<<*sw<<std::endl;
	#endif
	TypeWavefront::iterator it = findWave(sw);
	if (it!=wavefront->end()) {
		wavefront->erase(it);
	}else{
		#ifdef DEBUG
		std::cout<<"!ERROR deleteWave() Wave not found in wavefront"<<std::endl;
		#endif
	}
}

bool Skeleton2D::isWavePairInserted(Skeleton2DWaveSP const& sw1, Skeleton2DWaveSP const& sw2){
	if (sw1!=sw2){
		std::pair<TypeMapWavePair::iterator,bool> ret;
		ret=insertedWavePairs.insert(std::pair<TypeWavePair,bool>(std::make_pair(sw1,sw2),true));
		if (ret.second){
			ret=insertedWavePairs.insert(std::pair<TypeWavePair,bool>(std::make_pair(sw2,sw1),true));
			if (ret.second) return false;
		}
	}
	return true;
}

bool Skeleton2D::isInEqualRange(Skeleton2DWaveSP const& sw, Skeleton2DWaveSP const& swcomp){
	TypeWavefrontRange range = wavefront->equal_range(sw);
	for (TypeWavefront::iterator it=range.first; it!=range.second; ++it){
		#ifdef DEBUG
		std::cout<<"Equal range="<<*(*it)<<std::endl;
		#endif
		if ((*it)==swcomp) return true;
	}
	return false;
}

bool Skeleton2D::isUpperNeighbour(Skeleton2DWaveSP const& sw, Skeleton2DWaveSP const& swup){
	TypeWavefrontRange range = getUpperNeighbours(sw);
	for (TypeWavefront::iterator it=range.first; it!=range.second; ++it){
		#ifdef DEBUG
		std::cout<<"UpWave="<<*(*it)<<std::endl;
		#endif
		if ((*it)==swup) return true;
	}
	return false;
}

bool Skeleton2D::isLowerNeighbour(Skeleton2DWaveSP const& sw, Skeleton2DWaveSP const& swlow){
	TypeWavefrontRange range = getLowerNeighbours(sw);
	for (TypeWavefront::iterator it=range.first; it!=range.second; ++it){
		#ifdef DEBUG
		std::cout<<"LowWave="<<*(*it)<<std::endl;
		#endif
		if ((*it)==swlow) return true;
	}
	return false;
}

Skeleton2D::TypeWavefront::iterator Skeleton2D::insertWave(Skeleton2DWaveSP const& sw){
	#ifdef DEBUG
	std::cout<<"+Inserting wave"<<*sw<<std::endl;
	#endif
	return wavefront->insert(sw);
}

void Skeleton2D::insertNeighbourWaveSpikeEvents(Skeleton2DWaveSP const& sw){
	TypeWavefrontRange rangeeq = wavefront->equal_range(sw);
	for (TypeWavefront::iterator it = rangeeq.first; it!=rangeeq.second; ++it)
		insertSpikeEvent(sw, *it);
	TypeWavefrontRange rangelow = getLowerNeighbours(sw);
	for (TypeWavefront::iterator it = rangelow.first; it!=rangelow.second; ++it)
		insertSpikeEvent(sw, *it);
	TypeWavefrontRange rangeup = getUpperNeighbours(sw);
	for (TypeWavefront::iterator it = rangeup.first; it!=rangeup.second; ++it)
		insertSpikeEvent(sw, *it);
}

void Skeleton2D::insertEqualNeighbourSpikeEvents(Skeleton2DWaveSP const& sw, Skeleton2DEventPoint::Skeleton2DEventTypePoint type){
	TypeWavefrontRange rangeeq = wavefront->equal_range(sw);
	for (TypeWavefront::iterator it = rangeeq.first; it!=rangeeq.second; ++it)
		if (insertSpikeEvent(sw, *it, type)==-1) return;
}

void Skeleton2D::insertUpperNeighbourSpikeEvents(Skeleton2DWaveSP const& sw, Skeleton2DEventPoint::Skeleton2DEventTypePoint type){
	TypeWavefrontRange rangeup = getUpperNeighbours(sw);
	for (TypeWavefront::iterator it = rangeup.first; it!=rangeup.second; ++it)
		if (insertSpikeEvent(sw, *it, type)==-1) return;
}

void Skeleton2D::insertLowerNeighbourSpikeEvents(Skeleton2DWaveSP const& sw, Skeleton2DEventPoint::Skeleton2DEventTypePoint type){
	TypeWavefrontRange rangelow = getLowerNeighbours(sw);
	for (TypeWavefront::iterator it = rangelow.first; it!=rangelow.second; ++it)
		if (insertSpikeEvent(sw, *it, type)==-1) return;
}

Skeleton2D::TypeWavefront::iterator Skeleton2D::getWavefrontLowerBound(Number y, bool lowerOrdinate){
	TypeWavefront::iterator it = wavefront->end();
	if (!wavefront->empty()){
		Skeleton2DWaveSP sw (new Skeleton2DWave(y, lowerOrdinate));
		it = wavefront->lower_bound(sw);
	}
	return it;
}

Skeleton2D::TypeWavefront::iterator Skeleton2D::getWavefrontUpperBound(Number y, bool lowerOrdinate){
	TypeWavefront::iterator it = wavefront->end();
	if (!wavefront->empty()){
		Skeleton2DWaveSP sw (new Skeleton2DWave(y, lowerOrdinate));
		it = wavefront->upper_bound(sw);
	}
	return it;
}

Skeleton2D::TypeWavefrontRange Skeleton2D::getUpperNeighbours(Skeleton2DWaveSP const& sw){
	TypeWavefrontRange range;
	TypeWavefront::iterator it = wavefront->upper_bound(sw);
	if (it!=wavefront->end()) range = wavefront->equal_range(*it);
	else range = std::make_pair(wavefront->end(),wavefront->end());
	return range;
}

Skeleton2D::TypeWavefrontRange Skeleton2D::getLowerNeighbours(Skeleton2DWaveSP const& sw){
	TypeWavefrontRange range;
	TypeWavefront::iterator it = wavefront->lower_bound(sw);
	if (it!=wavefront->begin()) {
		--it;
		range = wavefront->equal_range(*it);
	}else range = std::make_pair(wavefront->end(),wavefront->end());
	return range;
}

Skeleton2DEdge* Skeleton2D::getUpperNeighbourSharedEdge(Skeleton2DWaveSP const& sw){
	TypeWavefrontRange range=getUpperNeighbours(sw);
	for (TypeWavefront::iterator it=range.first; it!=range.second; ++it){
		Skeleton2DEdge* sharedEdgeUp=NULL;
		classifyEdges(sw,*it,sharedEdgeUp);
		if (sharedEdgeUp!=NULL)
			return sharedEdgeUp;
	}
	return NULL;
}

Skeleton2DEdge* Skeleton2D::getLowerNeighbourSharedEdge(Skeleton2DWaveSP const& sw){
	TypeWavefrontRange range = getLowerNeighbours(sw);
	for (TypeWavefront::iterator it=range.first; it!=range.second; ++it){
		Skeleton2DEdge* sharedEdgeLow=NULL;
		classifyEdges(sw,*it,sharedEdgeLow);
		if (sharedEdgeLow!=NULL)
			return sharedEdgeLow;
	}
	return NULL;
}

Skeleton2DEdge* Skeleton2D::getIntersectedEdge(Skeleton2DWaveSP const& sw, Skeleton2DEdge* siteEdge, Skeleton2DEdge* noSiteEdge, bool upper){
	Number Di1[6], Di2[6];
	bool Di1b = getSquare3Edges(siteEdge, noSiteEdge, sw->getEdge1(), Di1);
	bool Di2b = getSquare3Edges(siteEdge, noSiteEdge, sw->getEdge2(), Di2);
	if (!Di1b && !Di2b){
		std::cerr<<"!ERROR getIntersection() two test squares are invalid"<<std::endl;
		exit(-1);
	}else if (Di1b && !Di2b){
		return sw->getEdge1();
	}else if (!Di1b && Di2b){
		return sw->getEdge2();
	}else{
		TypeWavefront::iterator it = findWave(sw);
		if (it!=wavefront->end()){
			Skeleton2DEdge* sharedEdgeUp = getUpperNeighbourSharedEdge(sw);
			Skeleton2DEdge* sharedEdgeLow = getLowerNeighbourSharedEdge(sw);
			#ifdef DEBUG
			if (sharedEdgeUp!=NULL)
				std::cout<<"SharedEdedgeUp="<<*sharedEdgeUp<<std::endl;
			if (sharedEdgeLow!=NULL) 
				std::cout<<"SharedEdgeLow="<<*sharedEdgeLow<<std::endl;
			#endif
			if ((sharedEdgeUp!=NULL)&&(sharedEdgeLow!=NULL)&&(sharedEdgeUp==sharedEdgeLow)){
				if (sw->getEdge1()==sharedEdgeUp) return sw->getEdge2();
				else return sw->getEdge1();
			}else if ((sharedEdgeUp!=NULL)&&(upper)) {
				return sharedEdgeUp;
			}else if ((sharedEdgeLow!=NULL)&&(!upper)) {
				return sharedEdgeLow;
			}else if ((sharedEdgeLow==NULL)&&(!upper)) {
				if (sw->getEdge1()==sharedEdgeUp) return sw->getEdge2();
				else  return sw->getEdge1();
			}else if ((sharedEdgeUp==NULL)&&(upper)) {
				if (sw->getEdge1()==sharedEdgeLow) {
					return sw->getEdge2();
				}else{
					return sw->getEdge1();
				}
			}
		}
	}
	return NULL;
}

void Skeleton2D::insertWaves(std::vector<Skeleton2DWaveSP>& waves){
	std::vector<Skeleton2DWaveSP>::iterator itWave;
	#ifdef DEBUG
	std::cout<<"Insert new spike bisectors"<<std::endl;
	#endif
	for (itWave=waves.begin() ; itWave != waves.end(); ++itWave)
		insertWave(*itWave);
	#ifdef DEBUG
	std::cout<<"Insert new spike events"<<std::endl;
	#endif
	for (itWave=waves.begin() ; itWave != waves.end(); ++itWave)
		insertNeighbourWaveSpikeEvents(*itWave);
	waves.clear();
}

void Skeleton2D::pruneGraph(){
	graph->pruneGraph(1);
}

#ifdef DEBUG
void Skeleton2D::printWavefrontInfo(){
	std::cout<<"+Wavefront status:"<<std::endl;
	TypeWavefront::iterator it;
	int pos=0;
	for (it=wavefront->begin() ; it != wavefront->end(); ++it){
		Skeleton2DWaveSP sw = *it;
		std::cout<<"\t"<<pos<<" "<<*sw<<std::endl;
		pos++;
	}
}

void Skeleton2D::printInfo(){
	polygon->printInfo();
	graph->printInfo();
	printStatistics();
}

void Skeleton2D::updateWavefrontStatistics(){
	maxWavefrontSize = max(maxWavefrontSize,wavefront->size());
	sumWavefrontSizes+=wavefront->size();
}

void Skeleton2D::printStatistics(){
	std::cout<<"Statistics"<<std::endl;
	std::cout<<"\t"<<numProcessedEvents<<" processed events"<<std::endl;
	std::cout<<"\t"<<numAcceptedEvents<<" accepted events" <<std::endl;
	std::cout<<"\t"<<(numProcessedEvents - numAcceptedEvents)<<" rejected events" <<std::endl;
	std::cout<<"\t"<<(int)(((double)numAcceptedEvents*100)/(double)numProcessedEvents)<<"% accepted events" <<std::endl;
	std::cout<<"\t"<<maxWavefrontSize<<" maximum wavefront size"<<std::endl;
	std::cout<<"\t"<<(int)(((double) sumWavefrontSizes)/(double)numProcessedEvents)<<" medium wavefront size"<<std::endl;
}

#endif
