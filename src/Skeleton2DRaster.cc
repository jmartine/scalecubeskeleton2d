#include "Skeleton2DRaster.h"

Skeleton2DRaster::Skeleton2DRaster(int xmin, int ymin, int xmax, int ymax) : bitmap( boost::extents[0][0] ){
	// this->xmin=xmin; this->xmax=xmax; this->ymin=ymin; this->ymax=ymax;
	// int nx, ny;
	if (xmin<0) {
		offsetx=-xmin;
		nx=xmax-xmin;
	} else {
		offsetx=0;
		nx = xmax;
	}
	if (ymin<0){
		offsety=-ymin;
		ny=ymax-ymin;
	}else{
		offsety=0;
		ny = ymax;
	}

	nx = xmax;
	ny = ymax;
	offsetx = offsety = 0;
	bitmap.resize(boost::extents[ny][nx]);
}

void Skeleton2DRaster::addVertex(int ax, int ay, int c){
	if(ay + offsety < ny and ax + offsetx  < nx) {
		if (ay + offsety > 0 and ax + offsetx > 0) {
			bitmap[ay + offsety][ax + offsetx] = c;
		}
	}
}

void Skeleton2DRaster::addEdge(double ax, double ay, double bx, double by, int c){
 	if (ay>by && (ay-floor(ay))==0 ) ay -= 0.5;
	if (by>ay && (by-floor(by))==0 ) by -= 0.5;
	if (ax>bx && (ax-floor(ax))==0 ) ax -= 0.5;
	if (bx>ax && (bx-floor(bx))==0 ) bx -= 0.5;
	int incx, incy, size;
	if (bx>ax) incx=+1;
	else if (bx<ax) incx=-1;
	else incx =0;
	if (by>ay) incy=+1;
	else if (by<ay) incy=-1;
	else incy =0;
	if (incx==0) size=abs(by-ay);
	else if (incy==0) size=abs(bx-ax);
	else size=abs(by-ay);
	int x=ax,y=ay;
	for (int i=0; i<=size; i++){
		addVertex(x, y, c);
		x += incx;
		y += incy;
	}
}

void Skeleton2DRaster::exportRaster(string path){
	ofstream  filePpm;
	filePpm.open(path.c_str());

	
	filePpm << "P6" << endl
	// filePpm << "P3" << endl
			<< nx << " " << ny << endl
			<< 255 << endl;

	char _0 = 0;
	char _255 = 255;
	for(int i = ny-1; i >= 0; --i) {
		for(int j = 0; j < nx; ++j) {
			switch (bitmap[i][j]) {
				case EMPTY:
					filePpm << _255 << _255 << _255;
					// filePpm << "255 255 255 ";
					break;
				case SKELETON:
					filePpm << _255 << _0 << _0;
					// filePpm << "255 0 0 ";
					break;
				case BORDER:
					filePpm << _0 << _0 << _0;
					// filePpm << "0 0 0 ";
					break;
			}
		}
	}
	filePpm.close();
}

Skeleton2DRaster::~Skeleton2DRaster(){
}
