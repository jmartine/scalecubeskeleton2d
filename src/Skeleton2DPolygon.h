#ifndef __SKELETON2DPOLYGON_H
#define __SKELETON2DPOLYGON_H

#include "Skeleton2DEdge.h"
#include "Skeleton2DVertex.h"
#include "Skeleton2DUtil.h"
#include <algorithm>
#include <vector>
#include <set>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "float.h"

using namespace std;

class Skeleton2DPolygon:public vector<Skeleton2DVertex*>{ 
	//Vertexs stored CCW
	private:
		Number xmin,xmax,ymin,ymax;
		vector<Skeleton2DEdge*> edges; 
		vector<Vertex> tempVertices; 
		set<int> range;
		void fillEdgeList();
		void updateBoundingBox();
		void addVertex(string x, string y);
		void addVertex(Number x, Number y);
		void processTxtFile(string filename, bool preprocess);
		void processBrepFile(string filename, bool preprocess);
		void processBrepVertex(string line, bool preprocess);
		void processBrepFace(string line);
		void updateShift(string x);
		int vertexId;
		int minShift, maxShift;
		Number shift;
	public:
		Skeleton2DPolygon(string filename);
		~Skeleton2DPolygon();
		void printInfo();
		bool isOrthogonal();
		void printEdgesInfo();
		Number getXmin() const {return xmin;}
		Number getYmin() const {return ymin;}
		Number getXmax() const {return xmax;}
		Number getYmax() const {return ymax;}
		Number getShift() {return shift; }
		void addToRaster(Skeleton2DRaster* raster);
		void appendToSvg(string path, int xmax, int ymax);
		
		//! \return next vertex with respect to input vertex identifier
		Skeleton2DVertex* getNextVertex(int id);
		
		//! \return previous vertex with respect to input vertex identifier
		Skeleton2DVertex* getPrevVertex(int id);
		
		//! \return next edge with respect to input edge identifier
		Skeleton2DEdge* getNextEdge(int id);
		
		//! \return previous edge with respect to input edge identifier
		Skeleton2DEdge* getPrevEdge(int id);
		
		//! \return next vertex identifier with respect to input vertex identifier
		int getNextVertexId(int id);
		
		//! \return previous vertex identifier with respect to input vertex identifier
		int getPrevVertexId(int id);
		
		//! \return edge associated to the input edge identifier
		Skeleton2DEdge* getEdge(int id);
};

#endif
