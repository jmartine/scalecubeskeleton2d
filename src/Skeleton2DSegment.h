#ifndef __SKELETON2DSEGMENT_H
#define __SKELETON2DSEGMENT_H

#include "Skeleton2D.h"
#include "Skeleton2DParams.h"
#include "Skeleton2DEventSegment.h"
#include "Skeleton2DEventPoint.h"
#include "Skeleton2DGlobals.h"

class Skeleton2DSegment : public Skeleton2D {
	public:
		~Skeleton2DSegment();
		typedef std::priority_queue<Skeleton2DEventSegment, std::vector<Skeleton2DEventSegment>, Skeleton2DEventCompareSegment> TypeEventQueueSegment;
		Skeleton2DSegment(Skeleton2DParams &PARAMS) : Skeleton2D(PARAMS){ events=NULL; numSiteEvents=0;};
		virtual bool injectNextEvent();
		virtual void initializeWavefront();
		virtual void initializeEvents();
		virtual int insertSpikeEvent(Skeleton2DWaveSP sw1, Skeleton2DWaveSP sw2, Skeleton2DEventPoint::Skeleton2DEventTypePoint type = Skeleton2DEventPoint::SPIKE_NORMAL_EVENT);
	private:
		//! \brief Retrieves the intersection between a intersected wave and the bisector defined by a site edge and no site edge
		//! \param sw intersected wave in wavefront
		//! \param siteEdge site edge of the bisector
		//! \param noSiteEdge no site edge of the bisector
		//! \param sv vertex origin of the bisector
		//! \param upper indicates if is an upper or lower bisector
		void getIntersection(Skeleton2DWaveSP const& sw, Skeleton2DEdge* siteEdge, Skeleton2DEdge* noSiteEdge, Skeleton2DVertex* sv, bool upper);
		TypeEventQueueSegment* events;
		int numSiteEvents;
		std::vector<Skeleton2DWaveSP> wavesToInsertImmediate;
};

#endif
