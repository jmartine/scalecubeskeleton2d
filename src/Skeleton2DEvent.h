#ifndef __SKELETON2DEVENT_H
#define __SKELETON2DEVENT_H

#include <iostream>
#include "Skeleton2DUtil.h"
#include "Skeleton2DWave.h"

class Skeleton2DEvent{
	public:
		Number getX() const { return x;}
		Skeleton2DWaveSP getWave1() const {return sw1;}
		Skeleton2DWaveSP getWave2() const {return sw2;}
		Skeleton2DEdge* getSharedEdge() const { return sharedEdge; }
		Skeleton2DEdge* getEdge1() const { return edge1; }
		Skeleton2DEdge* getEdge2() const { return edge2; }
		Number* getSquare() {return Di;}
	protected:
		Number x; 
		Number Di[6];
		Skeleton2DWaveSP sw1, sw2;
		Skeleton2DEdge *sharedEdge, *edge1, *edge2;
};

#endif
