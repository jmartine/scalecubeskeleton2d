#ifndef __SKELETON2DVERTEX_H
#define __SKELETON2DVERTEX_H

#include "Skeleton2DGraph.h"
#include "Skeleton2DGlobals.h"


class Skeleton2DVertex {
	public:
		Vertex v;
		Skeleton2DGraph::VertexId graphId;
		int id;
};

#endif
