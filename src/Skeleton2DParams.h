#ifndef __SKELETON2DPARAMS_H
#define __SKELETON2DPARAMS_H
#include <string>

using namespace std;

class Skeleton2DParams{
	public:
		Skeleton2DParams();
		string filename, svgOutput, ppmOutput;
		bool pointMode, cubeskeleton, outputBorders;
		double multDistScale;
		int boundx, boundy;
};

#endif
