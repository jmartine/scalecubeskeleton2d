#ifndef __SKELETON2D_H
#define __SKELETON2D_H

#include <queue>
#include <vector>
#include <set>
#include <map>
#include <string>
#include "Skeleton2DVertex.h"
#include "Skeleton2DEdge.h"
#include "Skeleton2DWave.h"
#include "Skeleton2DPolygon.h"
#include "Skeleton2DGraph.h"
#include "Skeleton2DEvent.h"
#include "Skeleton2DEventPoint.h"
#include "Skeleton2DUtil.h"
#include "Skeleton2DParams.h"
#include "Skeleton2DGlobals.h"

class Skeleton2D{
	protected:
		typedef std::multiset<Skeleton2DWaveSP,bool(*)(Skeleton2DWaveSP,Skeleton2DWaveSP)> TypeWavefront;
		typedef std::pair<TypeWavefront::iterator, TypeWavefront::iterator> TypeWavefrontRange;
		typedef std::pair<Skeleton2DWaveSP, Skeleton2DWaveSP> TypeWavePair;
		typedef std::map<TypeWavePair,bool> TypeMapWavePair;
		
		Skeleton2DPolygon* polygon;
		TypeWavefront* wavefront;
		Skeleton2DGraph* graph;
		Skeleton2DParams params;
		TypeMapWavePair insertedWavePairs;
		std::vector<Skeleton2DWaveSP> wavesToInsert;
		bool valid;
		Number last_x;
		
		//! \brief Inserts a wave in the wavefront
		//! \param sw Wave to insert
		//! \return an iterator pointing the inserted element
		TypeWavefront::iterator insertWave(Skeleton2DWaveSP const& sw);
		
		//! \brief Inserts neighbour spike events induced by a wave
		//! \param sw Wave inducing spike bisectors 
		void insertNeighbourWaveSpikeEvents(Skeleton2DWaveSP const& sw);
		
		//! \brief Inserts neighbour spike events induced by the input and equal waves in wavefront
		//! \param sw Wave inducing spike bisectors 
		void insertEqualNeighbourSpikeEvents(Skeleton2DWaveSP const& sw, Skeleton2DEventPoint::Skeleton2DEventTypePoint type = Skeleton2DEventPoint::SPIKE_NORMAL_EVENT);
		
		//! \brief Inserts neighbour spike events induced by the input wave and upper waves in wavefront
		//! \param sw Wave inducing spike bisectors 
		void insertUpperNeighbourSpikeEvents(Skeleton2DWaveSP const& sw, Skeleton2DEventPoint::Skeleton2DEventTypePoint type = Skeleton2DEventPoint::SPIKE_NORMAL_EVENT);
		
		//! \brief Inserts neighbour spike events induced by the input and lower waves in wavefront
		//! \param sw Wave inducing spike bisectors 
		void insertLowerNeighbourSpikeEvents(Skeleton2DWaveSP const& sw, Skeleton2DEventPoint::Skeleton2DEventTypePoint type = Skeleton2DEventPoint::SPIKE_NORMAL_EVENT);
		
		//! \brief Inserts an spike event defined by two waves that must share an edge 
		//! \param sw1 First wave
		//! \param sw2 Second wave
		virtual int insertSpikeEvent(Skeleton2DWaveSP sw1, Skeleton2DWaveSP sw2, Skeleton2DEventPoint::Skeleton2DEventTypePoint type = Skeleton2DEventPoint::SPIKE_NORMAL_EVENT) = 0;
		
		//! \brief Inserts a collection of waves in the wavefront and generates induced spike events
		//! \param waves Collection of waves
		void insertWaves(std::vector<Skeleton2DWaveSP> &waves);
		
		//! \brief Search the intersected edge of a wave sw
		//! \param sw Wave intersected
		//! \param siteEdge Site edge of origin intersection
		//! \param noSiteEdge No site edge of origin intersection
		//! \param upper Indicates if is an upper or lower intersection
		//! \return the intersected edge (NULL if not found)
		Skeleton2DEdge* getIntersectedEdge(Skeleton2DWaveSP const& sw, Skeleton2DEdge* siteEdge, Skeleton2DEdge* noSiteEdge, bool upper);
		
		//! \brief Returns lower bound wave intersected by a bisector defined by its y coordinate and the direction 
		//! \param y y-coordinate of bisector
		//! \param lowerOrdinate Direction of bisector (upper or lower)
		//! \return the intersected wave
		TypeWavefront::iterator getWavefrontLowerBound(Number y, bool lowerOrdinate);
		
		//! \brief Returns upper bound wave intersected by a bisector defined by its y coordinate and the direction 
		//! \param y y-coordinate of bisector
		//! \param lowerOrdinate Direction of bisector (upper or lower)
		//! \return the intersected wave
		TypeWavefront::iterator getWavefrontUpperBound(Number y, bool lowerOrdinate);
		
		//! \brief Returns the range of upper waves in the wavefront respect to the input wave
		//! \param sw Reference wave
		//! \return a pair of iterators pointing begin and end of range
		TypeWavefrontRange getUpperNeighbours(Skeleton2DWaveSP const& sw);
		
		//! \brief Returns the range of lower waves in the wavefront respect to the input wave
		//! \param sw Reference wave
		//! \return a pair of iterators pointing begin and end of range
		TypeWavefrontRange getLowerNeighbours(Skeleton2DWaveSP const& sw);
		
		//! \brief Returns the edge shared with the upper waves in the wavefront
		//! \param sw Reference wave
		//! \return a shared edge (NULL if does not exist)
		Skeleton2DEdge* getUpperNeighbourSharedEdge(Skeleton2DWaveSP const& sw);
		
		//! \brief Returns the edge shared with the lower waves in the wavefront
		//! \param sw Reference wave
		//! \return a shared edge (NULL if does not exist)
		Skeleton2DEdge* getLowerNeighbourSharedEdge(Skeleton2DWaveSP const& sw);
		
		//! \brief Checks if a wave is an upper neighbour of another wave
		//! \param sw Reference wave
		//! \param swup Wave supposed to be upper in wavefront
		//! \return true if is an upper neighbour and false otherwise
		bool isUpperNeighbour(Skeleton2DWaveSP const& sw, Skeleton2DWaveSP const& swup);
		
		//! \brief Checks if a wave is a lower neighbour of another wave
		//! \param sw Reference wave
		//! \param swup Wave supposed to be lower in wavefront
		//! \return true if is a lower neighbour and false otherwise
		bool isLowerNeighbour(Skeleton2DWaveSP const& sw, Skeleton2DWaveSP const& swlow);
		
		//! \brief Checks if a wave is a in the same range of another wave
		//! \param sw Reference wave
		//! \param swup Wave supposed to be in the same range of the wavefront
		//! \return true if is in the same range and false otherwise
		bool isInEqualRange(Skeleton2DWaveSP const& sw, Skeleton2DWaveSP const& swcomp);
		
		//! \brief Checks if a pair of waves have been generated before
		//! \return true if wave pair have been generated and false otherwise
		bool isWavePairInserted(Skeleton2DWaveSP const& sw1, Skeleton2DWaveSP const& sw2);
		
		//! \brief Finds a wave in the wavefront
		//! \param sw Wave to be found
		//! \return an iterator pointing the wavefront position (wavefront.end() if not found)
		TypeWavefront::iterator findWave(Skeleton2DWaveSP const& sw);
		
		//! \brief Delete a wave in the wavefront
		//! \param sw Wave to be deleted
		void deleteWave(Skeleton2DWaveSP const& sw);
		
		//! \brief Initializes the wavefront data structure
		virtual void initializeWavefront() = 0;
		
		//! \brief Initializes the event list
		virtual void initializeEvents() = 0;
		
		#ifdef DEBUG
		unsigned int numProcessedEvents, numAcceptedEvents, maxWavefrontSize, sumWavefrontSizes;
		//! \brief Prints in stdout the statistical information
		void printStatistics();
		
		//! \brief Updates the current statistical information
		void updateWavefrontStatistics();
		#endif
	public:
		//! \brief Skeleton2D constructor using input skeleton parameters
		Skeleton2D(Skeleton2DParams& params);
		
		//! \brief Skeleton2D default destructor
		virtual ~Skeleton2D();
		
		//! \brief Process the next pending event. Two methods available (segment/point)
		//! \return true if there are more events to process and false otherwise
		virtual bool injectNextEvent() = 0;
		
		//! \brief Initialize the structures and information used by the sweep algorithm
		void initialize();
		
		//! \brief Removes all the vertices with vertex degree lower than two of the skeleton
		void pruneGraph();
		
		//! \brief Rasterizes the computed skeleton
		void rasterGraph();
		
		//! \brief Checks the validity of the skeleton computation
		//! \return true if is valid and false otherwise
		bool isValid() const { return valid;}
		
		#ifdef DEBUG
		//! \brief Prints in stdout the current wavefront status
		void printWavefrontInfo();
		
		//! \brief Prints in stdout the polygon and skeleton information, and the current statistical data
		void printInfo();
		#endif
};

#endif
