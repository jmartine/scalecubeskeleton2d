#ifndef __SKELETON2DPOINT_H
#define __SKELETON2DPOINT_H

#include <climits>
#include "Skeleton2D.h"
#include "Skeleton2DParams.h"
#include "Skeleton2DEventPoint.h"

class Skeleton2DPoint : public Skeleton2D {
	public:
		~Skeleton2DPoint();
		typedef std::priority_queue<Skeleton2DEventPoint, std::vector<Skeleton2DEventPoint>, Skeleton2DEventComparePoint> TypeEventQueuePoint;
		Skeleton2DPoint(Skeleton2DParams &PARAMS) : Skeleton2D(PARAMS) {events=NULL, last_y=MAXNUMBER;};
		virtual bool injectNextEvent();
		virtual void initializeWavefront();
		virtual void initializeEvents();
		virtual int insertSpikeEvent(Skeleton2DWaveSP sw1, Skeleton2DWaveSP sw2, Skeleton2DEventPoint::Skeleton2DEventTypePoint type);
	private:
		//! \brief Creates a new bisector using two edges. If the edges do not define single square a constant square passed by parameter is used
		//! \param se1 first edge defining a bisector
		//! \param se2 second edge defining a bisector
		//! \param DiConst constant square used to classify the wave
		//! \param graphId last active graph id of the outgoing vertex
		//! \return the new wave created according the input parameters
		Skeleton2DWaveSP createNewWave(Skeleton2DEdge* se1, Skeleton2DEdge *se2, Number* DiConst, Skeleton2DGraph::VertexId graphId);
		Number last_y;
		TypeEventQueuePoint* events;
};

#endif
