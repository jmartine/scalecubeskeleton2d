#include "Skeleton2DPolygon.h"

using namespace std;

Skeleton2DPolygon::Skeleton2DPolygon(string filename){
	cout<<"* Loading orthogonal polygon... "<<std::flush;
	timeval iniTime, endTime;
	(void) gettimeofday(&iniTime, NULL);
	// get the extension
	string extension = filename.substr(filename.find_last_of(".")+1);
	vertexId=0;
	maxShift = 0;
	minShift = INT_MAX;
	if (extension.compare("txt")==0){
		processTxtFile(filename, true);
		shift = 2;
	    for (int i = 0; i < min(maxShift, minShift); ++i) {
	       shift *= 10;
	    }
		processTxtFile(filename, false);		
	}
	if (extension.compare("brep") == 0) {
		processBrepFile(filename, true);
		shift = 2;
	    for (int i = 0; i < min(maxShift, minShift); ++i) {
	       shift *= 10;
	    }
		processBrepFile(filename, false);
	}
	range.insert(vertexId);		
	updateBoundingBox();
	fillEdgeList();
	(void) gettimeofday(&endTime, NULL);
	std::cout<<getSeconds(iniTime, endTime)<<"s."<<std::endl;
}

Skeleton2DPolygon::~Skeleton2DPolygon(){
	for (unsigned int i=0; i<size(); i++) delete at(i);
	clear();
	for (unsigned int i=0; i<edges.size(); i++) delete edges[i];
	edges.clear();
	range.clear();
}

void Skeleton2DPolygon::processTxtFile(string filename, bool preprocess=false) {
	string line;
	ifstream file (filename.c_str());
	range.insert(vertexId);
	if (file.is_open()){
		while (! file.eof()){
			getline (file,line);
			if (line.compare("h")==0){
				range.insert(vertexId);
			}else if (line.size()>2){
				string x,y;
				size_t pos = line.find(" ");
				x = line.substr(0,pos);
				y = line.substr(pos+1);
				y = y.substr(0,y.rfind(" "));
				if(preprocess) {
					updateShift(x);
					updateShift(y);
				}
				else {
					addVertex(x, y);
				}
			}
		}
		file.close();
	}else cout<<"!ERROR Skeleton2DPolygon() Unable to open file"<<endl; 
}

void Skeleton2DPolygon::processBrepFile(string filename, bool preprocess=false) {
	string line;
	ifstream file (filename.c_str());
	if (file.is_open()){
		vertexId = 0;
		while (! file.eof()){
			getline (file,line);
			if (line.find("vertex") != string::npos){
				processBrepVertex(line, preprocess);
			} else if (line.find("ares") != string::npos and not preprocess) {
				processBrepFace(line);
			}
		}
		tempVertices.clear();
		file.close();
	}else cout<<"!ERROR Skeleton2DPolygon() Unable to open file"<<endl; 
}

void Skeleton2DPolygon::processBrepVertex(string line, bool preprocess) {
	// cout << "Process brep vertex, line=" << line << endl;
	int posIni = line.find("(");
	line = line.substr(posIni + 1);
	int posComma = line.find(",");
	string x = line.substr(0, posComma);
	line = line.substr(posComma + 1);
	int posComma2 = line.find(",");
	string y = line.substr(0, posComma2);
	if(preprocess) {
		updateShift(x);
		updateShift(y);
	}
	else {
		Vertex v{ convertToNumber(x, shift), convertToNumber(y, shift) };
		tempVertices.push_back(v);
	}
}

void Skeleton2DPolygon::processBrepFace(string line) {
	//cara99.poli.ares 	 ( 6024, 6027, 6026, 6025 )
	int posIni = line.find("(");
	line = line.substr(posIni + 1);
	//6024, 6027, 6026, 6025 )
	int comma = line.find(",");
	bool first = true;
	while(comma != -1) {
		string vertexIndex = line.substr(0, comma);
		int index = convertToInt(vertexIndex);
		if(first) {
			range.insert(vertexId);
			first = false;
		}
		Vertex v = tempVertices[index];
		addVertex(v.x, v.y);
		line = line.substr(comma + 1);
		comma = line.find(",");
	}
	// 6025 )
	int posPar = line.find(")");
	string vertexIndex = line.substr(0, posPar);
	int index = convertToInt(vertexIndex);
	Vertex v = tempVertices[index];
	addVertex(v.x, v.y);
}

void Skeleton2DPolygon::addVertex(string x, string y){
	addVertex(convertToNumber(x, shift), convertToNumber(y, shift));
}

void Skeleton2DPolygon::addVertex(Number x, Number y){
	Vertex v {x, y};
	Skeleton2DVertex* sv = new Skeleton2DVertex();
	sv->v=v;
	push_back(sv);
	++vertexId;
}

void Skeleton2DPolygon::updateShift(string x) {
	size_t comma = x.find(".");
	if(comma != string::npos and not (comma == x.size() - 2 and x[x.size()-1] == '0') ) {
		int max = x.size() - comma - 1;
		maxShift = max(maxShift, max);
		}

	double xD = convertTodouble(x);
	int min = 0;
	if(xD != 0) { //otherwise we'd enter and endless loop
		while(xD < MAXNUMBER and xD > MINNUMBER) {
			++min;
			xD *= 10;
		}
	}
	else min = INT_MAX; //so that 0 doesn't interfere
	minShift = min(minShift, min);
}

void Skeleton2DPolygon::printInfo(){
	cout<<"Model information"<<endl;
	cout<<"\tVertices="<<size()<<endl;
	cout<<"\tEdges="<<edges.size()<<endl;
	cout<<"\tBounding box=[("<<xmin<<","<<xmax<<"),("<<ymin<<","<<ymax<<")]"<<endl;
}

void Skeleton2DPolygon::updateBoundingBox(){
	xmin=ymin=MAXNUMBER;
	xmax=ymax=MINNUMBER;
	for (unsigned int id=0; id<size(); id++){
		Skeleton2DVertex* sv = at(id);
		if (sv->v.x<xmin) xmin=sv->v.x;
		if (sv->v.y<ymin) ymin=sv->v.y;
		if (sv->v.x>xmax) xmax=sv->v.x;
		if (sv->v.y>ymax) ymax=sv->v.y;
	}
}

void Skeleton2DPolygon::fillEdgeList(){
	for (unsigned int id=0; id<size(); id++){
		Skeleton2DVertex* sv = at(id);
		Skeleton2DVertex* svn = getNextVertex(id);
		sv->id=id;
		Skeleton2DEdge* se = new Skeleton2DEdge(id,sv,svn);
		edges.push_back(se);
	}
}

bool Skeleton2DPolygon::isOrthogonal(){
	set<int>::iterator it = range.begin();
	while (it!=range.end()){
		int begin=*it;
		++it;
		if (it!=range.end()){
			int end =*it;
			double lastx=0, lasty=0;
			for (int id=begin; id<end; id++){
				bool repeated=false;
				Vertex v = at(id)->v;
				Vertex vn = getNextVertex(id)->v;
				//avoid repeating 
				if ((id>begin) && (v.x==vn.x) && (v.x==lastx)){
					cout<<"!ERROR isOrthogonal() Repeated x coordinate"<<endl;
					cout << "vx=" << v.x << " vnx=" << vn.x << " lastx=" << lastx << endl; 
					this->erase(this->begin()+id);
					repeated=true;
					return false;
				}
				if ((id>begin) && (v.y==vn.y) && (v.y==lasty)){
					cout<<"!ERROR isOrthogonal() Repeated y coordinate"<<endl;
					this->erase(this->begin()+id);
					repeated=true;
					return false;
				}
				//Check orthogonality
				if (!repeated and v.x != vn.x and v.y != vn.y){
					cout<<v.x<<","<<v.y<<endl;
					cout<<vn.x<<","<<vn.y<<endl;
					cout<<"!ERROR isOrthogonal() 2D orthogon condition failed"<<endl;
					return false;
				}
				if (v.x==vn.x) lastx = v.x;
				if (v.y==vn.y) lasty = v.y;
			}
		}
	}
	return true;
}

void Skeleton2DPolygon::addToRaster(Skeleton2DRaster* raster) {
	for (Skeleton2DEdge * e : edges) {
		double s = shift;
		raster->addEdge(e->getV()->v.x/s, e->getV()->v.y/s, e->getVn()->v.x/s, e->getVn()->v.y/s, BORDER);
	}
}

void Skeleton2DPolygon::appendToSvg(string path, int xmax, int ymax) {
	fstream fileSvg;
	fileSvg.open(path.c_str(), fstream::out | fstream::app);
	fileSvg << "<g id=\"polygon\">"<<std::endl;
	int stroke_width = 1 + max(xmax, ymax)/1000;
	double s = shift;
	for (Skeleton2DEdge * e : edges) {
		double x1 = e->getV()->v.x/s;
		double y1 = e->getV()->v.y/s;
		double x2 = e->getVn()->v.x/s;
		double y2 = e->getVn()->v.y/s;
		fileSvg << "<line x1=\"" << x1 << "\" y1=\"" << ymax - y1 <<"\" x2=\"" << x2 <<"\" y2=\"" << ymax - y2 
				<< "\" style=\"stroke:rgb(0,0,0);stroke-width:" << stroke_width << "\" />" << endl;
	}
	fileSvg << "</g>" <<std::endl;
	fileSvg << "</svg>" << endl;
	fileSvg.close();
}

Skeleton2DVertex* Skeleton2DPolygon::getNextVertex(int id){
	return at(getNextVertexId(id));
}

Skeleton2DVertex* Skeleton2DPolygon::getPrevVertex(int id){
	return at(getPrevVertexId(id));
}

Skeleton2DEdge* Skeleton2DPolygon::getNextEdge(int id){
	return edges.at(getNextVertexId(id));
}

Skeleton2DEdge* Skeleton2DPolygon::getPrevEdge(int id){
	return edges.at(getPrevVertexId(id));
}

int Skeleton2DPolygon::getNextVertexId(int id){
	set<int>::iterator it = range.upper_bound(id);
	int upb = *it;
	--it; 
	int lowb = *it;
	return  lowb + ((id-lowb+1)%(upb-lowb));
}

int Skeleton2DPolygon::getPrevVertexId(int id){
	set<int>::iterator it = range.upper_bound(id);
	int upb = *it;
	--it; 
	int lowb = *it;
	int newid;
	if ((id-1)<lowb) newid= (upb-1);
	else newid= (id-1);
	return newid;
}

Skeleton2DEdge* Skeleton2DPolygon::getEdge(int id){
	return edges.at(id);
}
