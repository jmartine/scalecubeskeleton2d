#ifndef __SKELETON2DGLOBALS_H
#define __SKELETON2DGLOBALS_H

#include <sys/time.h>
#include <climits>

typedef long long Number;

const Number MAXNUMBER = LLONG_MAX;
const Number MINNUMBER = LLONG_MIN;

struct Vertex {
	Number x, y;
};

bool operator<(const Vertex& v, const Vertex& u);

double getSeconds(const timeval& iniTime, const timeval& endTime);

#endif
