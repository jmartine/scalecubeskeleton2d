
#coding: utf-8

import itertools
import bisect
import random
import Image, ImageDraw
from pixels import pixels
import sys
import os
from bitmap import Bitmap
_info = True


def printinfo(p, newline=True):
    if _info:
        print p,
        if newline:
            print


def parelles(iterable):
    """
    Iterador per tractar els elements d'una seq. per parelles.
    Sigui s = e0, e1, e2, e3,...
      parelles(s) = (e0,e1), (e2,e3), ...
    """
    args = [iter(iterable)]*2
    return itertools.izip(*args)


def finestra2(iterable):
    """
    Iterador per tractar una seq. amb finestra de 2.
    Sigui S = e0, e1, e2, e3, ...
      finestra2(s) = (e0,e1), (e1,e2), (e2,e3), ...
    """
    a, b = itertools.tee(iterable)
    b.next()
    return itertools.izip(a, b)


def cares_pixels(pixs):
    # obtenim les llistes d'arestes verticals i horitzontals
    printinfo("- Obtenint les llistes de vèrtexs...")
    lars1 = pixs.fvm()
    lars1.sort()
    lars2 = [(y,x) for (x,y) in lars1]
    lars2.sort()
    lars2 = [(y,x) for (x,y) in lars2]
    # cridem a la funció que fa l'escombrat a partir de les 2 llistes
    printinfo("- Formant les cares ...")
    lcf = forma_cares(lars1, lars2)
    printinfo("- Invertint els forats")
    for ef, pol in lcf:
        if ef=='F':
            pol.reverse()
    return lcf

def cares(lars1):
    # obtenim les llistes d'arestes verticals i horitzontals
    printinfo("- Obtenint les llistes de vèrtexs...")
    lars2 = [(y,x) for (x,y) in lars1]
    lars2.sort()
    lars2 = [(y,x) for (x,y) in lars2]
    # cridem a la funció que fa l'escombrat a partir de les 2 llistes
    printinfo("- Formant les cares ...")
    lcf = forma_cares(lars1, lars2)
    printinfo("- Invertint els forats")
    for ef, pol in lcf:
        if ef=='F':
            pol.reverse()
    return lcf

def forma_cares(lxy, lyx):
    """
    A partir d'una llista d'una llista d'arestes horitzontals i una de 
    verticals, reconstrueix (i retorna) totes les cares que formen.
    """
    # copiem la llista d'arestes horitzontals
    lescom = [(vi,vf) for (vi,vf) in parelles(lxy)]
    # fem l'escombrat de lescom per formar les cares i forats
    lpols = []          # llista de polígons que formem
    seccio = []         # secció horitzontal: [(y1,idcara1) , (y2,idcara2) ...]
    forats = {}         # (índexos dels) forats que té cada cara
    de_quina_cara = {}  # a quina cara pertany cada aresta horitzontal
    for vini, vfi in lescom:
        if (vini,vfi) not in de_quina_cara:
            # si és una aresta que no pertany a cap dels polígons reconstruïts
            idpol = len(lpols)
            seccio, idcara = xor1_seccio(seccio, vini, vfi, idpol)
            es_forat = idcara >= 0
            pol = forma_pol(lxy, lyx, es_forat)
            lpols.append( (pol, es_forat) )
            if es_forat:
                forats[idcara].append(idpol)
            else:
                idcara = idpol
                forats[idcara] = []
            afegeix_arestes_cara(pol, idcara, de_quina_cara)
        else:
            # és una aresta que pertany a un polígon ja reconstruït
            idcara = de_quina_cara[vini, vfi]
            seccio = xor2_seccio(seccio, vini, vfi, idcara)
    # formem la llista de polígons externs seguit (cadascun) dels seus forats
    lcar_for = []
    for idpol, polf in enumerate(lpols):
        pol, es_forat = polf
        if not es_forat:            
            lcar_for.append(('E', pol))
            lcar_for.extend([ ('F', lpols[idforat][0]) 
                              for idforat in forats[idpol]])
    return lcar_for


def afegeix_arestes_cara(pol, idcara, de_quina_cara):
    """
    Emmagatzema totes les arestes horitzontals de pol en el 
    diccionari de_quina_cara per més endavant poder-ho consultar
    """
    for vi,vf in finestra2(pol) :
        # si es horitzontal
        if vi[0]==vf[0]:
            # la desem, amb els vèrtexs ordenats
            if vi > vf:
                vi, vf = vf,vi
            de_quina_cara[vi,vf] = idcara


def xor1_seccio(seccio, vini, vfi, idpol):
    """
    Actualitza la secció afegint-hi el segment (vini, vfi), sabent que
    els extrems d'aquest segment no coincideixen amb cap element
    de la secció. Per tant, és una aresta "extrema" d'un polígon
    exterior o bé una aresta "extrema" d'un forat.
    El darrer paràmetre serveix per a determinar, en cas que es tracti 
    d'un forat, de quina cara ho és. 
    Retorna la nova secció i un enter que serà: 
      -1 si es tracta d'un polígon extern (cara);
      en cas contrari, l'identificador de la cara del qual és un forat.
    """
    yini = vini[1]
    yfi = vfi[1]
    idx = cerca_en_seccio(seccio, yfi)
    if idx%2 == 0:
        # és un polígon extern
        idcara = -1
    else:
        # és un forat
        idcara = seccio[idx][1]
        idpol = idcara
    seccio =  seccio[:idx] + [(yini,idpol)] + [(yfi,idpol)] + seccio[idx:]
    return seccio, idcara
        

def cerca_en_seccio(seccio, y):
    return bisect.bisect_left(seccio, (y,None))


def xor2_seccio(seccio, vini, vfi, idcara):
    """
    Actualitza la secció afegint-hi el segment (vini, vfi), sabent que
    es tracta d'un segment d'un polígon ja format.
    Retorna la secció actualitzada convenientment.
    """
    yini = vini[1]
    yfi = vfi[1]
    idx = cerca_en_seccio(seccio, yini)
    # fem la fusió de [yini,yfi] amb secció[idx:]
    sentinella = (vfi[1]+1, -1)
    interval1 = seccio[idx:] + [sentinella]
    interval2 = [(yini,idcara) , (yfi,idcara) , sentinella]
    e1 = interval1[0]
    e2 = interval2[0]
    nouint = seccio[:idx]
    while e2 != sentinella:
        if e1[0] < e2[0]:
            nouint.append(e1)
            interval1 = interval1[1:]
            e1 = interval1[0]
        elif e1[0] > e2[0]:
            nouint.append(e2)
            interval2 = interval2[1:]
            e2 = interval2[0]
        else:
            if e1[1] == e2[1]:
                interval1 = interval1[1:]
                e1 = interval1[0]
                interval2 = interval2[1:]
                e2 = interval2[0]
            else:
                if e2 == yfi:
                    nouint.append(e2)
                    interval2 = interval2[1:]
                    e2 = interval2[0]
                else:
                    nouint.append(e1)
                    interval1 = interval1[1:]
                    e1 = interval1[0]
    return nouint + interval1[:-1]


def forma_pol(lxy, lyx, es_forat):
    vant = lxy[0]
    vtx = lxy[1]
    pol = [vant, vtx]
    # esborrem la primera aresta de la llista lxy
    del lxy[0]
    del lxy[0]
    lv = (lxy, lyx)
    k = 1
    while vtx != pol[0]:
        idx = vfi_aresta(vtx, lv[k], k, vant, es_forat)
        # si no el trobem és perquè es tracta d'una aresta coplanar;
        # a la propera iteració el cercarem a l'altra llista
        if idx != None:
            vant = vtx
            vtx = lv[k][idx]
            pol.append(vtx)
            # esborrem l'aresta (els dos punts) de la llista
            if idx%2 == 1:
                idx = idx-1
            del lv[k][idx]
            del lv[k][idx]
        k = 1-k
    return pol


def cerca_dicotomica_lv(lv, vtx, ordre_xy):
    lo = 0
    hi = len(lv)
    if not ordre_xy:
        vtx = (vtx[1], vtx[0])
    while lo < hi:
        mid = (lo+hi)//2
        if ordre_xy:
            lvmid = lv[mid]
        else:
            lvmid = (lv[mid][1], lv[mid][0])
        if lvmid < vtx: 
            lo = mid+1
        else: 
            hi = mid
    return lo        

def vfi_aresta(vtx, lv, k, vant, es_forat):
    """
    Cerca el vèrtex vtx en la llista d'arestes lv i retorna l'índex de
    la parella corresponent a vtx
    Si hi ha més d'un candidat, decideix quin agafar segons si estem
    reconstruïnt un forat o un polígon extern. Per això es necessita vant,
    el vèrtex anterior del polígon que estem reconstruïnt. 
    Si vtx no pertany a lv, retorna -1.
    """
    idx = cerca_dicotomica_lv(lv, vtx, k==0)
    if lv[idx] != vtx:
        return None
    # si és un vèrtex inicial, retornem el final.
    if idx%2==0:
        return idx+1 
    # mirem si hi ha més d'un candidat (ssi es tracta d'un non-manifold)
    if idx+1 >= len(lv) or lv[idx] != lv[idx+1]:
        return idx-1
    # print "manifold"
    # print vant, vtx, es_forat
    x1, y1 = vant
    x2, y2 = vtx
    gir = (x1==x2 and y1<y2) or (x1>x2 and y1==y2)
    if es_forat != gir:
        return idx+2
    else:
        return idx-1

def coordToId(vCoord):
    return str(vIds[vCoord])

def print_cares(lcares):
    nf = 0
    nc = 0
    for ef, pol in lcares:
        nomCara = "cara" + str(nc) 
        nc += 1         
        print nomCara + ".pla.d\t: 0"      
        print nomCara + ".pla.n\t( 0, 0, 1 )"
        pol.reverse()
        if ef == 'F':
            nf += 1
            print nomCara + ".poli.ares \t ( " + ', '.join(map(coordToId, pol[:-1] )) + " )"
        else:
            print nomCara + ".poli.ares \t ( " + ', '.join(map(coordToId, pol[:-1] )) + " )"

        
    printinfo ("# Cares:", len(lcares) - nf)
    printinfo ("# Forats:", nf)
    printinfo ("Total cares:", len(lcares))
    return

def process_vertexs(lcares):
    i = 0
    for cara in lcares:
        vlist = cara[1]
        for vertex in vlist:
            if not vertex in vIds:
                vIds[vertex] = i
                print_vertex(vertex, i)
                i += 1

def print_vertex(vertex, i):
    print "vertex" + str(i) + "\t(" + str(vertex[0]) + ", " + str(vertex[1]) + ", -1)"     

vIds = {}

if __name__=="__main__":
    _info = False

    
    inFile = open(sys.argv[1], 'r')
    i = 0

    extension = os.path.splitext(sys.argv[1])[1];
    if extension.lower() == ".ppm" or extension.lower() == ".pgm" or extension.lower() == ".pbm":
        
        bmp = Bitmap()
        bmp.read_pbm(sys.argv[1])

        pix = bmp.binarize()

        printinfo("NetPBM file read")

        print "OBJECTE:poli"
        print "{"
        lcares = cares_pixels(pix)
        process_vertexs(lcares)
        print_cares(lcares)

        print "}"

    elif extension == ".vl":

        vertexs = []    
    
        print "OBJECTE:poli"
        print "{"

        for line in inFile: 
            t = line.rstrip().split(" ")
            t = tuple( (float(t[0]), float(t[1])) )
            vertexs.append( t )
            
            print_vertex(t, i)  
            vIds[t] = str(i)
            i += 1

        lcares = cares(vertexs)
        printinfo("- Calculant les cares i forats...")

        print_cares(lcares)
        print "}"

    else:
        printinfo("Wrong file format: Must be .vl or NetPBM (.ppm, .pgm, .pbm)")
    
    