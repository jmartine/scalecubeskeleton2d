
#coding: utf-8

import struct
from pixels import pixels

class Bitmap(object):

    def __init__(self, height=0, width=0):
        """
        Creates an empty bitmap
        """
        self.height = height
        self.width = width
        self.bitmap = []
        self.max_value = 1
        self.max_value_over = 1
        self.item_size = 1
        for i in range(0, height):
            self.bitmap.append( [0] * width )

    def process_value(self, i, j, value, overwrite):
        if overwrite:
            (r, g, b) = value 
            if not (r == self.max_value_over and r == g and r == b):
                (r, g, b) = (r*self.max_value/self.max_value_over, g*self.max_value/self.max_value_over, b*self.max_value/self.max_value_over)
                self.bitmap[self.height - i - 1][j] = (r, g, b)
        else:
            self.bitmap[self.height - i - 1][j] = value

    def read_pbm(self, fname, overwrite = False):
        f = open(fname)
        p_number = f.readline().strip()  #P1-P6

        dimline = f.readline().strip()
        while dimline.startswith('#'):
            dimline = f.readline().strip()
        dimensions = map(int, dimline.split())

        self.width = int(dimensions[0])
        self.height = int(dimensions[1])
        
        if not overwrite:     
            self.bitmap = [ [0] * self.width for k in range(self.height) ]

        i = j = 0
        if p_number.strip() == "P1":      
            line = f.readline()
            while line != "":
                for char in line.split():
                    self.process_value(i, j, 1-int(char), overwrite)
                    (i, j) = self.advance(i, j)
                line = f.readline()

        elif p_number.strip() == "P2":      
            if not overwrite:
                self.max_value = int(f.readline().strip())
            else:
                self.max_value_over = int(f.readline().strip())
            line = f.readline()
            while line != "":
                for char in line.split():
                    self.process_value(i, j, int(char), overwrite)
                    (i, j) = self.advance(i, j)
                line = f.readline()

        elif p_number.strip() == "P3":
            self.item_size = 3
            if not overwrite:
                self.max_value = int(f.readline().strip())
            else:
                self.max_value_over = int(f.readline().strip())
            rgb = 0
            line = f.readline()
            while line != "":
                for char in line.split():
                    if rgb == 0:
                        r = int(char)
                    elif rgb == 1:
                        g = int(char)
                    else:
                        b = int(char)
                        self.process_value(i, j, (r, g, b), overwrite)
                        (i, j) = self.advance(i, j)
                    rgb = (rgb + 1)%3
                line = f.readline()

        elif p_number.strip() == "P4":
            byte = f.read(1)
            while byte != "":
                integer = struct.unpack('B', byte)[0]
                bits = map(int, bin(integer)[2:])
                padding = [0] * (8 - len(bits))
                bits.reverse()
                bits.extend(padding)
                bits.reverse()
                for bit in bits:
                    self.process_value(i, j, 1-int(bit), overwrite)
                    (i, j) = self.advance(i, j)
                byte = f.read(1)
                    

        elif p_number.strip() == "P5":        
            if not overwrite:
                self.max_value = int(f.readline().strip())
            else:
                self.max_value_over = int(f.readline().strip())
            byte = f.read(1)
            while byte != "":
                integer = ord(struct.unpack('c', byte)[0])
                self.process_value(i, j, integer, overwrite)
                (i, j) = self.advance(i, j)
                byte = f.read(1)


        elif p_number.strip() == "P6":
            self.item_size = 3
            rgb = 0
            if not overwrite:
                self.max_value = int(f.readline().strip())
            else:
                self.max_value_over = int(f.readline().strip())
            byte = f.read(1)
            while byte != "":
                val = ord(struct.unpack('c', byte)[0])
                if rgb == 0:
                    r = val
                elif rgb == 1:
                    g = val
                else:
                    b = val
                    self.process_value(i, j, (r, g, b), overwrite)
                    (i, j) = self.advance(i, j)
                rgb = (rgb + 1)%3
                byte = f.read(1)
        f.close()
    
    def binarize_row(self, item):
        if self.item_size == 3:
            (r, g, b) = item
            return luminance(r, g, b) > self.max_value/2
        else:
            return item > self.max_value/2

    def binarize(self):
        pix = pixels()
        pix.ample = self.width
        pix.alt = self.height
        for row in self.bitmap:
            binrow = list(map(self.binarize_row, row))
            pix.lliure.append( binrow )
        

        return pix

    def write(self, path):
        f = open(path, "w")
        f.write("P3\n")
        f.write(str(self.width) + " " + str(self.height) + "\n")
        f.write(str(self.max_value) + "\n")
        for i in range(self.height):
            for j in range(self.width):
                (r, g, b) = self.bitmap[self.height - i - 1][j]
                f.write( str(r) + " " + str(g) + " " + str(b) + " ")
        f.close()

    def advance(self, i, j):
        j += 1
        if j >= self.width:
            j = 0
            i += 1
        return (i, j)

def luminance(r, g, b):
    return 0.2126*r + 0.7152*g + 0.0722*b

