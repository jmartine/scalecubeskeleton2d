
#coding: utf-8

import random
import struct
import Image, ImageDraw

class pixels(object):

    def __init__(self, alt=0, ample=0, buit=True):
        """
        Crea una pixelació de les mides indicades amb totes
        les posicions ocupades/desocupades en funció del booleà buit
        """
        self.alt = alt
        self.ample = ample
        self.lliure = []
        for i in range(0, alt):
            self.lliure.append( [buit] * ample )


    def dins_limits(self, f, c):
        """
        Comprova si la posicio (f,c) està dins dels límits
        """
        return 0 <= f < self.alt and 0 <= c < self.ample


    def write(self, nomf):
        """
        Escriu una pixelació en un fitxer
        """
        fitxer = open(nomf, 'w')
        fitxer.write(str(self.alt) + ' ' + str(self.ample) + '\n')
        fitxer.write(str(self))
        fitxer.close()


    def read(self, nomf):
        """
        Llegeix una pixelació d'un fitxer
        """
        fitxer = open(nomf, 'r')
        alt, ample = fitxer.readline().split()
        self.__init__(int(alt), int(ample))
        for fila in range(0, self.alt):
            linia = fitxer.readline()
            for columna in range(0, self.ample):
                self.lliure[fila][columna] = linia[columna] != 'X'
        fitxer.close()

        
    def itera_pixels(self):
        for fila in range(self.alt):
            for columna in range(self.ample):
                yield fila, columna, self.lliure[fila][columna]

    
    def omple(self, fini, cini, ffi, cfi):
        for fila in range(fini, ffi):
            for columna in range(cini, cfi):
                self.lliure[fila][columna] = False


    def buida(self, fini, cini, ffi, cfi):
        for fila in range(fini, ffi):
            for columna in range(cini, cfi):
                self.lliure[fila][columna] = True


    def __str__(self):
        s = ''
        for fila in range(self.alt):
            for columna in range(self.ample):
                if self.lliure[fila][columna]:
                    s += '.'
                else:
                    s += 'X'
            s += '\n'
        return s


    def random(self, alt=10, ample=20, densitat=50):
        self.__init__(alt,ample)
        for fila, columna, ple in self.itera_pixels():
            self.lliure[fila][columna] = random.uniform(0,100) > densitat


    def negatiu(self):
        for fila, columna, ple in self.itera_pixels():
            self.lliure[fila][columna] = not self.lliure[fila][columna]
        

    def imatgeRGB(self, escala=10):
        im = Image.new('RGB',(self.ample*escala + escala*2, 
                              self.alt*escala + escala*2))
        draw = ImageDraw.Draw(im)
        for fila, columna, ple in self.itera_pixels():
            color = (250,250,220) if ple else (0,250,0)
            draw.rectangle((fila*escala+escala, fila*escala+escala, 
                            fila*escala+escala*2, fila*escala+escala*2), fill=color)
        for fila in range(self.alt+1):
            draw.line((escala, fila*escala+escala, 
                       self.ample*escala+escala, fila*escala+escala),
                      fill = (100,100,100))
        for columna in range(self.ample+1):
            draw.line((columna*escala+escala, escala, 
                       columna*escala+escala, self.alt*escala + escala),
                      fill = (100,100,100))
        return im


    def tipus_node(self, f, c):
        """
        retorna el tipus de node que hi ha en les coordenades (f,c):
          0 = exterior
          1 = vèrtex tipus 1 (convex)
          2 = vèrtex non-manifold
          3 = vèrtex tipus 3 (ncau)
          4 = interior
          'A' = aresta (vertical o horitzontal)          
        """
        pixels = [True]*4
        n = 0
        for i, nf, nc in [(0,f-1,c-1), (1,f-1,c), (2,f,c-1), (3,f,c)]:
            ocupat = self.dins_limits(nf, nc) and not self.lliure[nf][nc]
            pixels[i] = ocupat
            if ocupat:
                n += 1
        if n == 2 and pixels[0] != pixels[3]:
            return 'A'     # és una aresta
        else:
            return n       # el nombre de pixels plens indica el tipus


    def evm(self):
        """
        Retorna la llista de vèrtexs extrems, ordenats
        (és a dir, el model evm)
        """
        lv = []
        for f in range(self.alt+1):
            for c in range(self.ample+1):
                if self.tipus_node(f, c) in [1,3]:
                    lv.append((f, c))
        return lv
        
    
    def fvm(self):
        """ 
        Retorna la llista d'arestes (parelles de vèrtexs) horitzontals,
        ordenades (és a dir, el "full vertex model" (fvm)
        """
        lv = []
        for f in range(self.alt+1):
            for c in range(self.ample+1):
                t = self.tipus_node(f, c) 
                if t in [1, 2, 3]:
                    # print "vertexadded", (c, f)
                    lv.append((c, f))
                if t == 2: # els vèrtexs non-manifold els dupliquem
                    lv.append((c, f))
        return lv



    def read_pbm(self, fname):
        f = open(fname)
            # data = [x for x in f if not x.startswith('#')] #remove comments
        p_number = f.readline().strip()  #P1-P6

        dimline = f.readline().strip()
        while dimline.startswith('#'):
            dimline = f.readline().strip()
        dimensions = map(int, dimline.split())

        self.alt = int(dimensions[0])
        self.ample = int(dimensions[1])             
        self.lliure = [ [0] * self.ample for x in range(self.alt) ]

        i = j = 0
        if p_number.strip() == "P1":      
            line = f.readline()
            while line != "":
                for char in line.split():
                    self.lliure[i][self.ample - j - 1] = 1-int(char)
                    (i, j) = advance(i, j, dimensions[0])
                line = f.readline()

        elif p_number.strip() == "P2":      
            max_value = int(f.readline().strip())
            line = f.readline()
            while line != "":
                for char in line.split():
                    print i, j, "-", i, self.ample - j - 1
                    self.lliure[i][self.ample - j - 1] = int(char) > max_value/2
                    (i, j) = advance(i, j, dimensions[0])
                line = f.readline()
                    
        elif p_number.strip() == "P3":
            max_value = int(f.readline().strip())
            rgb = 0
            line = f.readline()
            while line != "":
                for char in line.split():
                    if rgb == 0:
                        r = int(char)
                    elif rgb == 1:
                        g = int(char)
                    else:
                        b = int(char)
                        self.lliure[i][self.ample - j - 1] = (luminance(r, g, b) > max_value/2)
                        (i, j) = advance(i, j, dimensions[0])
                    rgb = (rgb + 1)%3
                line = f.readline()

        elif p_number.strip() == "P4":      
            byte = f.read(1)
            while byte != "":
                integer = struct.unpack('B', byte)[0]
                bits = map(int, bin(integer)[2:])
                padding = [0] * (8 - len(bits))
                bits.reverse()
                bits.extend(padding)
                bits.reverse()
                for bit in bits:
                    self.lliure[i][self.ample - j - 1] = 1-int(bit)
                    (i, j) = advance(i, j, dimensions[0])
                byte = f.read(1)

        elif p_number.strip() == "P5":        
            max_value = int(f.readline().strip())
            byte = f.read(1)
            while byte != "":
                integer = ord(struct.unpack('c', byte)[0])
                self.lliure[i][self.ample - j - 1] = integer > max_value/2
                (i, j) = advance(i, j, dimensions[0])
                byte = f.read(1)

        elif p_number.strip() == "P6":
            rgb = 0
            max_value = int(f.readline().strip())
            byte = f.read(1)
            while byte != "":
                val = ord(struct.unpack('c', byte)[0])
                if rgb == 0:
                    r = val
                elif rgb == 1:
                    g = val
                else:
                    b = val
                    self.lliure[i][self.ample - j - 1] = (luminance(r, g, b) > max_value/2)
                    (i, j) = advance(i, j, dimensions[0])

                rgb = (rgb + 1)%3
                byte = f.read(1)
                
        # self.write("pixelation.pix")

# def luminance(r, g, b):
#     return 0.2126*r + 0.7152*g + 0.0722*b

# def advance(i, j, dim):
#     i += 1
#     if i >= dim:
#         i = 0
#         j += 1
#     return (i, j)