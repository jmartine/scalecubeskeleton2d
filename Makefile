CC = @g++ 

HOME = .
SRCDIR = $(HOME)/src
OBJDIR  = $(HOME)/objects
INCDIR = $(HOME)/src

objects := $(patsubst $(SRCDIR)/%.cc,$(OBJDIR)/%.o,$(wildcard $(SRCDIR)/*.cc))
sources := $(wildcard $(SRCDIR)/*.cc)

FLAGS = -Wall -Wextra

CFLAGS =  $(FLAGS) -I$(INCDIR) -O2 -std=c++0x
LDFLAGS = $(FLAGS)

TARGETS = 2Dskeleton

.PHONY: all doc

all: $(TARGETS)
	
vpath %.o   $(OBJDIR)
vpath %.cc  $(SRCDIR)
vpath %.h   $(SRCDIR)

doc: 
	doxygen Doxyfile
	
$(OBJDIR)/%.o: $(SRCDIR)/%.cc
	@mkdir -p	$(OBJDIR)
	@echo Compiling $< ...
	$(CC) -c $(CFLAGS)  -o $@ $< 

2Dskeleton: $(objects)
	@echo Creating executable $@ ...
	$(CC) -o $@ $(objects) $(LDFLAGS) 
	
clean:
	rm -f $(OBJDIR)/*.o $(TARGETS) $(SRCDIR)/*~
