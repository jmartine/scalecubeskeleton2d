#!/bin/bash

function usage() {
    echo "Usage: ./scaleCubeSkeleton2D.sh −h −i [input file (txt,brep,ppm,pgm,pbm)] −s [output svg file] −p [output ppm file] −f [scale]"
}

SCALE=""
ppm=""
ppmFile=""

#optional parameters
while getopts "i:f:hs:p:r" arg; do
    case "${arg}" in
    h)
        usage
        exit
        ;;
    i)
        input=$OPTARG
        ;;
    f)
        SCALE=$OPTARG
        ;;
    s)
        svg="-s $OPTARG"
        ;;
    p)
        ppm="-p $OPTARG"
        ppmFile="$OPTARG"
        ;;
    r)
        r="-r"
        ;;
    
    \?)
        echo "WRONG PARAMETER" >&2
        usage
        ;;
  esac
done

if [ ! -e $input ]
then
	echo "Input file does not exist"
	usage
fi

filename=$(basename "$input")
extension="${filename##*.}"

if [ "$extension" == "ppm" ] || [ "$extension" == "pgm" ] || [ "$extension" == "pbm" ]
then
    echo "* Converting image file to brep"
    python toBrep2D/tobrep.py $input > "$input.brep"
    inputToUse="$input.brep"
    bound=`grep -a "^[0-9]" $input | head -n 1 | sed 's/ /,/g'` #gets the dimensions, inserts a comma between them
    bound="-b $bound"
else
    inputToUse="$input"
fi

if [ "$SCALE" != "" ]
then
    echo "* Computing cube skeleton..."
    ./2Dskeleton -i $inputToUse -f $SCALE 1>/dev/null
    echo "* Computing union of rectangles..."
		arch=`getconf LONG_BIT`
    ./bin/2DContourDC_$arch boxes.txt > boxes.vl # BoxUnion3D software
    rm boxes.txt
    python toBrep2D/tobrep.py boxes.vl > "$input.brep" # Orto-brep software
    inputToUse="$input.brep"
    rm boxes.vl
fi

echo -n "* Computing cube skeleton"
if [ "$SCALE" != "" ]
then
	echo -n " scale $SCALE"
fi
echo "..."
./2Dskeleton -i "$inputToUse" $ppm $svg $r "$bound" 1>/dev/null

if [[ "$ppm" != ""  && "$extension" == "ppm" || "$extension" == "pgm" || "$extension" == "pbm" ]]
then
    echo "* Overlaying skeleton on image"
    python toBrep2D/imageMerger.py "$input" "$ppmFile" "$ppmFile"
fi

